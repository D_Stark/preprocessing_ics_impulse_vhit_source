import numpy as np
from scipy.signal import find_peaks, peak_prominences
from math import isnan

def calculate_time(impulse, fps):
    ## calculate time series in msec
    T = 1/fps*1000
    time = []
    for i in range(len(impulse)):
        time.append(i*T)
    return time

def head_peak(head):
    ## find head peak velocity
    peak = np.where(head == np.max(head))
    return peak[0][0], head[peak[0][0]]

def head_onset(head, fps):
    ## extract head onset
    # calculate acceleration
    acc = np.diff(head)*fps
    # find head peak to limit acceleration peak search by this value
    peak_vel = np.where(head == np.max(head))
    #peaks, _ = find_peaks(acc, threshold=2000, distance=round(20/((1/fps)*1000)))
    peak_acc = np.where(acc == np.max(acc[:peak_vel[0][0]]))
    onset = peak_acc[0][0] - round(60/((1/fps)*1000))
    if onset >= 0:
        return onset
    else:
        return 0

def head_max_acc(head, fps):
    ## extract head onset
    # calculate acceleration
    acc = np.diff(head)*fps
    # find head peak to limit acceleration peak search by this value
    peak_vel = np.where(head == np.max(head))
    #peaks, _ = find_peaks(acc, threshold=2000, distance=round(20/((1/fps)*1000)))
    peak_acc = np.where(acc == np.max(acc[:peak_vel[0][0]]))
    onset = peak_acc[0][0] - round(60/((1/fps)*1000))
    if onset >= 0:
        return onset
    else:
        return 0

def head_offset(head):
    ## extract head offset
    # finds index after head peak where head changes sign
    peak = np.where(head == np.max(head))
    for i in range(peak[0][0], len(head)):
        if head[i]*head[i-1] < 0:
            return i
    return 1

def head_less_th(head, th):
    ## check if head peak < th
    peak = np.where(head == np.max(head))
    if head[peak[0][0]] < th:
        return 1
    return 0

def head_peak_3sdt(traces, fps):
    ## get interval calculated as:
    # 1) mean of the interval 80msec prior to and 120msec after peak head calculated for each impulse
    # 2) MEAN of all such means of all impulses
    # 3) std of all such means of all impulses
    # 4) interval [MEAN - 3std, MEAN + 3std]
    if traces != []:
        # get head peaks
        peaks = [np.where(traces[i][0] == np.max(traces[i][0])) for i in range(len(traces))]
        # take 80msec prior to and 120msec after peak head
        intervals = [[peaks[i][0][0] - round(80 / ((1 / fps) * 1000)), peaks[i][0][0] + round(120 / ((1 / fps) * 1000))]
                     for i in range(len(traces))]
        # MEAN of all means calculated within this interval
        mean = np.mean([np.mean(traces[i][0][intervals[i][0]:intervals[i][1]]) for i in range(len(traces))])
        # std of all means calculated within this interval
        std = np.std([np.mean(traces[i][0][intervals[i][0]:intervals[i][1]]) for i in range(len(traces))])
        return [mean - 3*std, mean + 3*std]
    else:
        return None

def head_out_3sd(head, interval, fps):
    ## check if each impulse is within interval calculated in head_peak_3sdt
    peak = np.where(head == np.max(head))
    mean = np.mean(head[peak[0][0] - round(80 / ((1 / fps) * 1000)):peak[0][0] + round(120 / ((1 / fps) * 1000))])
    if mean > interval[0] and mean < interval[1]:
        return 0
    return 1

def head_no_offset(offset):
    ## check if head offset is absent
    if offset == 1:
        return 1
    else:
        return 0

def head_bounce(head):
    ## check if head bounce > 50% of head peak
    peak = np.max(head)
    bounce = np.min(head)
    if bounce/peak >= 0.5:
        return 1
    else:
        return 0

def saccades(eye_vel, th_min, fps, th):
    ## extract saccades
    # th_min - minial index for saccade latencies
    # get eye velocity local peaks
    # find positive peaks
    peaks_vel_pos, _ = find_peaks(eye_vel, height=th, distance=round(20/((1/fps)*1000)),
                                         prominence=40,
                                         width=(round(5/((1/fps)*1000)), round(70/((1/fps)*1000))))
    # get peak prominence indexes as [peak_ind, left_base, right_base]
    prom_ind_pos = peak_prominences(eye_vel, peaks_vel_pos)
    # get ratio of left to right base prominences
    prom_rel_pos = [(prom_ind_pos[0][i] - eye_vel[prom_ind_pos[1][i]])/
                    (prom_ind_pos[0][i] - eye_vel[prom_ind_pos[2][i]])
                    for i in range(len(prom_ind_pos[1]))]
    # delete peak which prominence ratio is out of [0.33, 3]
    ind_to_del = []
    for i in range(len(prom_rel_pos)):
        if prom_rel_pos[i] < 0.33 or prom_rel_pos[i] > 3:
            ind_to_del.append(i)
    peaks_vel_pos = np.delete(peaks_vel_pos, ind_to_del)

    # find negative peaks
    peaks_vel_neg, _ = find_peaks([element * (-1) for element in eye_vel], height=th,
                                         distance=round(20/((1/fps)*1000)), prominence=40,
                                         width=(round(5/((1/fps)*1000)), round(100/((1/fps)*1000))))
    # get peak prominence indexes as [peak_ind, left_base, right_base]
    prom_ind_neg = peak_prominences([element * (-1) for element in eye_vel], peaks_vel_neg)
    # get values of prominences
    prom_rel_neg = [abs((-prom_ind_neg[0][i] - eye_vel[prom_ind_neg[1][i]])/
                        (-prom_ind_neg[0][i] - eye_vel[prom_ind_neg[2][i]]))
                    for i in range(len(prom_ind_neg[1]))]
    # delete peak which prominence is out of [0.33, 3]
    ind_to_del = []
    for i in range(len(prom_rel_neg)):
        if prom_rel_neg[i] < 0.33 or prom_rel_neg[i] > 3:
            ind_to_del.append(i)
    peaks_vel_neg = np.delete(peaks_vel_neg, ind_to_del)

    # combine al peaks
    peaks_vel = np.concatenate((peaks_vel_pos, peaks_vel_neg))
    # # calculate eye acceleration
    # acc = np.diff(eye_vel) * fps
    # # filter acc by mean filter with window k
    # k = round(8 / ((1 / fps) * 1000))
    # kern = np.ones(2 * k + 1) / (2 * k + 1)
    # acc = np.convolve(acc, kern, mode='same')
    # # get eye acceleration local peaks > 0
    # peaks_acc_pos, _ = find_peaks(acc)
    # # even in time with velocity by +1
    # peaks_acc_pos = np.add(peaks_acc_pos, 1)
    # # get eye acceleration local peaks < 0
    # peaks_acc_neg, _ = find_peaks(-acc)
    # # even in time with velocity by +1
    # peaks_acc_neg = np.add(peaks_acc_neg, 1)
    #
    # # get closest acceleration local picks to each velocity local peak
    # peaks_acc_pos_closest = map(lambda y: min(peaks_acc_pos, key=lambda x: abs(x - y)), peaks_vel)
    # peaks_acc_neg_closest = map(lambda y: min(peaks_acc_neg, key=lambda x: abs(x - y)), peaks_vel)
    # # join all closest acceleration peaks
    # peaks_acc_closest = list(map(lambda x: sorted(list(x)), zip(peaks_acc_pos_closest, peaks_acc_neg_closest)))
    # find onset for each saccade as either a pit or point of sign change
    onset = []
    for i in range(len(peaks_vel)):
        j = peaks_vel[i]-1 #peaks_acc_closest[i][0]
        while j > 0:
            if j-1 == 0 or eye_vel[j]*eye_vel[j-1] < 0:
                onset.append(j-1)
                break
            else:
                if (eye_vel[peaks_vel[i]] > 0 and eye_vel[j] < eye_vel[j-1]):
                    onset.append(j)
                    break
                else:
                    if (eye_vel[peaks_vel[i]] < 0 and eye_vel[j] > eye_vel[j-1]):
                        onset.append(j)
                        break
                    else:
                        j -= 1

    # find offset for each saccade as either pit or point of sign change
    offset = []
    for i in range(len(peaks_vel)):
        j = peaks_vel[i]+1#peaks_acc_closest[i][1]
        while j <= len(eye_vel)-2:
            if j+1 == len(eye_vel)-2 or eye_vel[j]*eye_vel[j+1] < 0:
                offset.append(j+1)
                break
            else:
                if (eye_vel[peaks_vel[i]] > 0 and eye_vel[j] < eye_vel[j+1]):
                    offset.append(j)
                    break
                else:
                    if (eye_vel[peaks_vel[i]] < 0 and eye_vel[j] > eye_vel[j+1]):
                        offset.append(j)
                        break
                    else:
                        j += 1

    # join onsets and offsets for all detected saccades
    onset_offset_all = list(map(lambda x: sorted(list(x)), zip(onset, offset)))

    # check if prominence based on found onsets and offsets is within [0.33, 3]
    # also check if onset is after th_min and take such saccades
    onset_offset = []
    for i in range(len(onset_offset_all)):
        if 0.33 <= abs((eye_vel[peaks_vel[i]] - eye_vel[onset_offset_all[i][0]])/\
                       (eye_vel[peaks_vel[i]] - eye_vel[onset_offset_all[i][1]])) <= 3:
                #and onset_offset_all[i][0] > th_min:
            onset_offset.append(onset_offset_all[i])

    # return indexes of velocity peaks, velocity magnitudes, and indexes of closest acceleration peaks
    #return peaks_vel, list(map(lambda x: eye_vel[x], peaks_vel)), peaks_acc_closest, onset, offset
    return onset_offset

def saccade_peak(eye_vel, onset, offset, fps):
    ## extract absolute index of saccade peak
    peak_vel = np.where(list(map(abs, eye_vel[round(onset*fps/1000):round(offset*fps/1000)])) == \
                        np.max(list(map(abs, eye_vel[round(onset*fps/1000):round(offset*fps/1000)]))))
    return round(peak_vel[0][0]*(1/fps)*1000) + onset

def calculate_gain(head, eye, onset, offset, saccades, fps):
    head2 = head.copy()
    eye2 = eye.copy()
    # replace head and eye saccade point by 0
    for i in saccades.keys():
        end = round(saccades[i][1].get()*fps/1000)
        if end > len(head2):
            end = len(head2)
        for j in range(round(saccades[i][0].get()*fps/1000), end):
            head2[j] = 0
            eye2[j] = 0

    # calculate gain
    head_sum = sum(head2[onset:offset])
    if head_sum != 0:
        return round(sum(eye2[onset:offset])/sum(head2[onset:offset]), 2)
    else:
        return None

def calculate_PR1_latency(latency_orig, take_auto, take_manual):
    # PR for first order saccades calcaulated using saccade latencies
    # take onsets of first saccades
    latency = latency_orig.copy()
    if latency != []:
        # first remove impulses which are not accepted by the program (4 conditions) or by manual selection
        # create var to store bad impulses
        bad_impuls = []
        for i in range(len(latency)):
            if sum(take_auto[i]) != 0 or take_manual[i].get() != 1:
                bad_impuls.append(i)
        # delete these bas=d impulses
        latency = [latency[i] for i in range(len(latency)) if i not in bad_impuls]
        latency_I = [sorted([imp[key][0].get() for key in imp.keys()])[0] for imp in latency if imp != {}]
        # find outliers: latencies of first order saccades outside -60ms+mean or 80ms+mean
        mean = np.mean(latency_I)
        outliers = [i for i in range(len(latency_I)) if latency_I[i] < mean - 60 or latency_I[i] > mean + 80]
        if len(outliers) <= 2:
            latency_I = [latency_I[i] for i in range(len(latency_I)) if i not in outliers]
        mean = np.mean(latency_I)
        n = len(latency_I)
        if mean != 0 and isnan(mean)==False:
            return round(np.std(latency_I)/mean*100), n
        else:
            return None, None
    else:
        return None, None

def calculate_PR1_peak(peaks_orig, take_auto, take_manual):
    # PR for first order saccades calcaulated using latencies of saccade peaks
    # take onsets of first saccades
    peaks = peaks_orig.copy()
    if peaks != []:
        # first remove impulses which are not accepted by the program (4 conditions) or by manual selection
        # create var to store bad impulses
        bad_impuls = []
        for i in range(len(peaks)):
            if sum(take_auto[i]) != 0 or take_manual[i].get() != 1:
                bad_impuls.append(i)
        # delete these bas=d impulses
        peaks = [peaks[i] for i in range(len(peaks)) if i not in bad_impuls]
        # take the first saccades
        peaks_I = [sorted([imp[key] for key in imp.keys()])[0] for imp in peaks if imp != {}]
        # find outliers: latencies of first order saccades outside -60ms+mean or 80ms+mean
        mean = np.mean(peaks_I)
        outliers = [i for i in range(len(peaks_I)) if peaks_I[i] < mean - 60 or peaks_I[i] > mean + 80]
        if len(outliers) <= 2:
            peaks_I = [peaks_I[i] for i in range(len(peaks_I)) if i not in outliers]
        mean = np.mean(peaks_I)
        n = len(peaks_I)
        if mean != 0 and isnan(mean)==False:
            return round(np.std(peaks_I)/mean*100), n
        else:
            return None, None
    else:
        return None, None

def calculate_PR2_latency(latency_orig, take_auto, take_manual):
    # PR for second order saccades
    # take onsets of second saccades
    latency_II = []
    latency = latency_orig.copy()
    if latency != []:
        # first remove impulses which are not accepted by the program (4 conditions) or by manual selection
        # create var to store bad impulses
        bad_impuls = []
        for i in range(len(latency)):
            if sum(take_auto[i]) != 0 or take_manual[i].get() != 1:
                bad_impuls.append(i)
        # delete these bas=d impulses
        peaks = [latency[i] for i in range(len(latency)) if i not in bad_impuls]
        for imp in latency:
            if imp != {}:
                temp = sorted([imp[key][0].get() for key in imp.keys()])
                if len(temp)>=2:
                    latency_II.append(temp[1])
        # find outliers: latencies of first order saccades outside -60ms+mean or 80ms+mean
        mean = np.mean(latency_II)
        outliers = [i for i in range(len(latency_II)) if latency_II[i] < mean - 60 or latency_II[i] > mean + 80]
        if len(outliers) <= 2:
            latency_II = [latency_II[i] for i in range(len(latency_II)) if i not in outliers]
        mean = np.mean(latency_II)
        n = len(latency_II)
        if mean != 0 and isnan(mean)==False:
            return round(np.std(latency_II)/mean*100), n
        else:
            return None, None
    else:
        return None, None

def calculate_PR2_peak(peaks_orig, take_auto, take_manual):
    # PR for second order saccades
    # take onsets of second saccades
    peaks_II = []
    peaks = peaks_orig.copy()
    if peaks != []:
        # first remove impulses which are not accepted by the program (4 conditions) or by manual selection
        # create var to store bad impulses
        bad_impuls = []
        for i in range(len(peaks)):
            if sum(take_auto[i]) != 0 or take_manual[i].get() != 1:
                bad_impuls.append(i)
        # delete these bas=d impulses
        peaks = [peaks[i] for i in range(len(peaks)) if i not in bad_impuls]
        for imp in peaks:
            if imp != {}:
                temp = sorted([imp[key] for key in imp.keys()])
                if len(temp) >= 2:
                    peaks_II.append(temp[1])
        # find outliers: latencies of first order saccades outside -60ms+mean or 80ms+mean
        mean = np.mean(peaks_II)
        outliers = [i for i in range(len(peaks_II)) if peaks_II[i] < mean - 60 or peaks_II[i] > mean + 80]
        if len(outliers) <= 2:
            peaks_II = [peaks_II[i] for i in range(len(peaks_II)) if i not in outliers]
        mean = np.mean(peaks_II)
        n = len(peaks_II)
        if mean != 0 and isnan(mean)==False:
            return round(np.std(peaks_II)/mean*100), n
        else:
            return None, None
    else:
        return None, None

def calculate_PR_global(PR1, PR2, N1, N2):
    # global PR for first and second order saccades
    pr1 = PR1
    pr2 = PR2
    n1 = N1
    n2 = N2

    if n1 is None:
        n1 = 0
        pr1 = 0

    if n2 is None:
        n2 = 0
        pr2 = 0

    if n1 != 0:
        if n2 != 0:
            ind = 1 - n1/(n1 + n2)
        else:
            ind = 0
    else:
         ind = 0

    pr_global = 2.5*(0.8*pr1 + 0.2*pr2)
    if pr_global > 35: pr_global = pr_global - 30*ind
    if pr_global > 100: pr_global = 100
    return round(pr_global)


