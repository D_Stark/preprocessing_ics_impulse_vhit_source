from matplotlib.figure import Figure
import tkinter as tk
from PIL import Image
from matplotlib.font_manager import FontProperties
import analysis

def set_font(self):
    # set font for labels
    self.font = FontProperties()
    self.font.set_family('serif')
    self.font.set_name('times new roman')

def create_canvas_frames(self):
    # create frames to pack into these frames 4 canvases
    # (which are created in plotting.py) presenting both individual trace and trial for LEFT and RIGHT
    self.frame_canvas_imp_LEFT = tk.Frame(self.Frame_Oto)
    self.frame_canvas_imp_RIGHT = tk.Frame(self.Frame_Oto)
    self.frame_canvas_trial_LEFT = tk.Frame(self.Frame_Oto)
    self.frame_canvas_trial_RIGHT = tk.Frame(self.Frame_Oto)

    # pack these frames
    self.frame_canvas_imp_LEFT.grid(row=5, column=0)
    self.frame_canvas_imp_RIGHT.grid(row=5, column=2)
    self.frame_canvas_trial_LEFT.grid(row=7, column=0)
    self.frame_canvas_trial_RIGHT.grid(row=7, column=2)

def create_figures(self):
    # the figure that will contain the plot
    self.fig_imp_LEFT = Figure(figsize=(4.5, 2.6), dpi=100)
    self.fig_trial_LEFT = Figure(figsize=(4.5, 2.6), dpi=100)
    self.fig_imp_RIGHT = Figure(figsize=(4.5, 2.6), dpi=100)
    self.fig_trial_RIGHT = Figure(figsize=(4.5, 2.6), dpi=100)
    # set default windows color
    self.fig_imp_LEFT.set_facecolor("#F0F0F0")
    self.fig_trial_LEFT.set_facecolor("#F0F0F0")
    self.fig_imp_RIGHT.set_facecolor("#F0F0F0")
    self.fig_trial_RIGHT.set_facecolor("#F0F0F0")

def graph_figures_imp_LEFT(self):
    # adding the subplot and plotting
    # plot individual impulse for LEFT
    # clear figure
    self.fig_imp_LEFT.clear()
    self.fig_imp_LEFT.set_tight_layout(True)
    self.plot_imp_LEFT = self.fig_imp_LEFT.add_subplot(111)

    self.plot_imp_LEFT.plot(self.time_LEFT[self.name_order][self.imp_order_LEFT],
                            self.traces_LEFT[self.name_order][self.imp_order_LEFT][0],
                            "darkorange",
                            #self.time_LEFT[self.name_order][self.imp_order_LEFT],
                            #self.traces_LEFT[self.name_order][self.imp_order_LEFT][1],
                            #"o", "cornflowerblue",
                            [self.time_LEFT[self.name_order][self.imp_order_LEFT][
                                 self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][0]],
                             self.time_LEFT[self.name_order][self.imp_order_LEFT][
                                 self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][0]]],
                            [-50, 100],
                            "blue",
                            [self.time_LEFT[self.name_order][self.imp_order_LEFT][
                                 self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][1]],
                             self.time_LEFT[self.name_order][self.imp_order_LEFT][
                                 self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][1]]],
                            [-50, 100],
                            "red")

    # plot eye velocity as points
    self.plot_imp_LEFT.plot(self.time_LEFT[self.name_order][self.imp_order_LEFT],
                            self.traces_LEFT[self.name_order][self.imp_order_LEFT][1],
                            "o", color="cornflowerblue", markersize=2.5)

    for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
        self.plot_imp_LEFT.plot(# get time
                                self.time_LEFT[self.name_order][self.imp_order_LEFT]
                                [round(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get()*self.fps[self.name_order]/1000):
                                 round(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get()*self.fps[self.name_order]/1000)],
                                # draw piece of eye velocity corresponding to each saccade
                                self.traces_LEFT[self.name_order][self.imp_order_LEFT][1]
                                [round(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get()*self.fps[self.name_order]/1000):
                                 round(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get()*self.fps[self.name_order]/1000)],
                                "o", color=self.colors[key], markersize=2.5)

    # add text showing gains and how impulse fits different inclusion criteria
    self.plot_imp_LEFT.text(0.98, 0.7, 'Head peak < {}: {}'.format(self.var_head_th.get(),
                                                                  bool(self.var_imp_take_auto_LEFT[self.name_order][self.imp_order_LEFT][0])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)
    self.plot_imp_LEFT.text(0.98, 0.8, 'Head peak out of ±3sd: {}'.format(bool(self.var_imp_take_auto_LEFT[self.name_order][self.imp_order_LEFT][1])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)
    self.plot_imp_LEFT.text(0.98, 0.6, 'No offset: {}'.format(bool(self.var_imp_take_auto_LEFT[self.name_order][self.imp_order_LEFT][2])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)
    self.plot_imp_LEFT.text(0.98, 0.9, 'Bounce > 50% head peak: {}'.format(bool(self.var_imp_take_auto_LEFT[self.name_order][self.imp_order_LEFT][3])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)
    self.plot_imp_LEFT.text(0.98, 0.4, 'Device gain: {}'.format(self.gain_device_LEFT[self.name_order][self.imp_order_LEFT]),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)
    self.plot_imp_LEFT.text(0.98, 0.5, 'Manual gain: {}'.format(self.gain_manual_LEFT[self.name_order][self.imp_order_LEFT]),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_LEFT.transAxes, fontsize=8)

    self.plot_imp_LEFT.format_cursor_data = lambda e: ""
    # set title and axes labels
    self.plot_imp_LEFT.set_title("Leftward", fontproperties=self.font)
    self.plot_imp_LEFT.set_xlabel('Time, msec', fontproperties=self.font)
    self.plot_imp_LEFT.set_ylabel('Velocity, deg/sec', fontproperties=self.font)
    # set default windows color
    self.plot_imp_LEFT.set_facecolor("#F0F0F0")

def graph_figures_trial_LEFT(self):
    # plot trail for LEFT

    self.fig_trial_LEFT.clear()
    self.fig_trial_LEFT.set_tight_layout(True)
    # plot head
    for i in range(len(self.time_LEFT[self.name_order])):
        self.plot_trial_LEFT = self.fig_trial_LEFT
        self.plot_trial_LEFT.add_subplot(111).plot(self.time_LEFT[self.name_order][i],
                                                 self.traces_LEFT[self.name_order][i][0], "darkorange")

    # plot eye
    for i in range(len(self.time_LEFT[self.name_order])):
        self.plot_trial_LEFT = self.fig_trial_LEFT.add_subplot(111)
        self.plot_trial_LEFT.plot(self.time_LEFT[self.name_order][i],
                                self.traces_LEFT[self.name_order][i][1], "dodgerblue")

        # plot saccades
        for key in self.var_onset_offset_LEFT[self.name_order][i].keys():
            self.plot_trial_LEFT.plot(
                # get time
                self.time_LEFT[self.name_order][i][round(self.var_onset_offset_LEFT[self.name_order][i][key][0].get() * self.fps[self.name_order] / 1000):
                                                    round(self.var_onset_offset_LEFT[self.name_order][i][key][1].get() * self.fps[self.name_order] / 1000)],
                # draw piece of eye velocity corresponding to each saccade
                self.traces_LEFT[self.name_order][i][1][round(self.var_onset_offset_LEFT[self.name_order][i][key][0].
                                                               get() * self.fps[self.name_order] / 1000):
                                                         round(self.var_onset_offset_LEFT[self.name_order][i][key][1].
                                                               get() * self.fps[self.name_order] / 1000)],
                "red")
    # add all three PR-scores
    self.plot_trial_LEFT.text(0.98, 0.9, 'PR1: {}'.format(self.PR1_LEFT[self.name_order][0]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_LEFT.transAxes,
                              fontsize=8)
    self.plot_trial_LEFT.text(0.98, 0.8, 'PR2: {}'.format(self.PR2_LEFT[self.name_order][0]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_LEFT.transAxes,
                              fontsize=8)
    self.plot_trial_LEFT.text(0.98, 0.7, 'PR global: {}%'.format(self.PR_global_LEFT[self.name_order]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_LEFT.transAxes,
                              fontsize=8)

    self.plot_trial_LEFT.set_xlabel('Time, msec', fontproperties=self.font)
    self.plot_trial_LEFT.set_ylabel('Velocity, deg/sec', fontproperties=self.font)

    # set default windows color
    self.plot_trial_LEFT.set_facecolor("#F0F0F0")

def graph_figures_imp_RIGHT(self):
    # plot individual impulse for RIGHT
    self.fig_imp_RIGHT.clear()
    self.fig_imp_RIGHT.set_tight_layout(True)
    self.plot_imp_RIGHT = self.fig_imp_RIGHT.add_subplot(111)

    self.plot_imp_RIGHT.plot(self.time_RIGHT[self.name_order][self.imp_order_RIGHT],
                                 self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][0],
                                 "darkorange",

                             [self.time_RIGHT[self.name_order][self.imp_order_RIGHT][
                                  self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][0]],
                              self.time_RIGHT[self.name_order][self.imp_order_RIGHT][
                                  self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][0]]],
                             [-50, 100],
                             "blue",
                             [self.time_RIGHT[self.name_order][self.imp_order_RIGHT][
                                  self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][1]],
                              self.time_RIGHT[self.name_order][self.imp_order_RIGHT][
                                  self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][1]]],
                             [-50, 100],
                             "red"
                             )

    # draw eye velocity
    self.plot_imp_RIGHT.plot(self.time_RIGHT[self.name_order][self.imp_order_RIGHT],
                             self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][1],
                             "o", color="cornflowerblue", markersize=2.5)

    for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
        self.plot_imp_RIGHT.plot(# get time
                                self.time_RIGHT[self.name_order][self.imp_order_RIGHT]
                                [round(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get()*self.fps[self.name_order]/1000):
                                 round(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get()*self.fps[self.name_order]/1000)],
                                # draw piece of eye velocity corresponding to each saccade
                                self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][1]
                                [round(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get()*self.fps[self.name_order]/1000):
                                 round(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get()*self.fps[self.name_order]/1000)],
                                "o", color=self.colors[key], markersize=2.5)

    # add text showing how impulse fits different inclusion criteria
    self.plot_imp_RIGHT.text(0.98, 0.7, 'Head peak < {}: {}'.format(self.var_head_th.get(),
                                                                  bool(self.var_imp_take_auto_RIGHT[self.name_order][self.imp_order_RIGHT][0])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)
    self.plot_imp_RIGHT.text(0.98, 0.8, 'Head peak out of ±3sd: {}'.format(bool(self.var_imp_take_auto_RIGHT[self.name_order][self.imp_order_RIGHT][1])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)
    self.plot_imp_RIGHT.text(0.98, 0.6, 'No offset: {}'.format(bool(self.var_imp_take_auto_RIGHT[self.name_order][self.imp_order_RIGHT][2])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)
    self.plot_imp_RIGHT.text(0.98, 0.9, 'Bounce > 50% head peak: {}'.format(bool(self.var_imp_take_auto_RIGHT[self.name_order][self.imp_order_RIGHT][3])),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)
    self.plot_imp_RIGHT.text(0.98, 0.4, 'Device gain: {}'.format(self.gain_device_RIGHT[self.name_order][self.imp_order_RIGHT]),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)
    self.plot_imp_RIGHT.text(0.98, 0.5, 'Manual gain: {}'.format(self.gain_manual_RIGHT[self.name_order][self.imp_order_RIGHT]),
                            horizontalalignment='right',
                            verticalalignment='center',
                            transform=self.plot_imp_RIGHT.transAxes, fontsize=8)

    # set title and axes labels
    self.plot_imp_RIGHT.set_title("Rightward", fontproperties=self.font)
    self.plot_imp_RIGHT.set_xlabel('Time, msec', fontproperties=self.font)
    self.plot_imp_RIGHT.set_ylabel('Velocity, deg/sec', fontproperties=self.font)

    # set default windows color
    self.plot_imp_RIGHT.set_facecolor("#F0F0F0")

def graph_figures_trial_RIGHT(self):
    # plot trail for RIGHT

    self.fig_trial_RIGHT.clear()
    self.fig_trial_RIGHT.set_tight_layout(True)
    # plot head
    for i in range(len(self.time_RIGHT[self.name_order])):
        self.plot_trial_RIGHT = self.fig_trial_RIGHT
        self.plot_trial_RIGHT.add_subplot(111).plot(self.time_RIGHT[self.name_order][i],
                                                    self.traces_RIGHT[self.name_order][i][0], "darkorange")

    # plot eye
    for i in range(len(self.time_RIGHT[self.name_order])):
        self.plot_trial_RIGHT = self.fig_trial_RIGHT.add_subplot(111)
        self.plot_trial_RIGHT.plot(self.time_RIGHT[self.name_order][i],
                                   self.traces_RIGHT[self.name_order][i][1], "dodgerblue")
        # plot saccades
        for key in self.var_onset_offset_RIGHT[self.name_order][i].keys():
            self.plot_trial_RIGHT.plot(
                # get time
                self.time_RIGHT[self.name_order][i][round(self.var_onset_offset_RIGHT[self.name_order][i][key][0].get() * self.fps[self.name_order] / 1000):
                                                    round(self.var_onset_offset_RIGHT[self.name_order][i][key][1].get() * self.fps[self.name_order] / 1000)],
                # draw piece of eye velocity corresponding to each saccade
                self.traces_RIGHT[self.name_order][i][1][round(self.var_onset_offset_RIGHT[self.name_order][i][key][0].
                                                               get() * self.fps[self.name_order] / 1000):
                                                         round(self.var_onset_offset_RIGHT[self.name_order][i][key][1].
                                                               get() * self.fps[self.name_order] / 1000)],
                "red")

    # add all three PR-scores
    self.plot_trial_RIGHT.text(0.98, 0.9, 'PR1: {}'.format(self.PR1_RIGHT[self.name_order][0]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_RIGHT.transAxes,
                              fontsize=8)
    self.plot_trial_RIGHT.text(0.98, 0.8, 'PR2: {}'.format(self.PR2_RIGHT[self.name_order][0]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_RIGHT.transAxes,
                              fontsize=8)
    self.plot_trial_RIGHT.text(0.98, 0.7, 'PR global: {}%'.format(self.PR_global_RIGHT[self.name_order]),
                              horizontalalignment='right',
                              verticalalignment='center',
                              transform=self.plot_trial_RIGHT.transAxes,
                              fontsize=8)

    self.plot_trial_RIGHT.set_xlabel('Time, msec', fontproperties=self.font)
    self.plot_trial_RIGHT.set_ylabel('Velocity, deg/sec', fontproperties=self.font)

    # set default windows color
    self.plot_trial_RIGHT.set_facecolor("#F0F0F0")

def plot_no_trial(self):
    # adding the subplot and plotting
    # but first import No_Test_Found.png
    self.img_no_data = Image.open("Pictures/No_Test_Found.png")

    # plot No_Test_Found.png on first canvas
    self.fig_imp_LEFT.clear()
    self.fig_trial_LEFT.clear()
    self.fig_imp_RIGHT.clear()
    self.fig_trial_RIGHT.clear()
    # make figure fully fit canvas
    self.fig_imp_LEFT.set_tight_layout(True)
    self.fig_trial_LEFT.set_tight_layout(True)
    self.fig_imp_RIGHT.set_tight_layout(True)
    self.fig_trial_RIGHT.set_tight_layout(True)
    # add img to figures, plot, and remove axes
    self.plot_imp_LEFT = self.fig_imp_LEFT.add_subplot(111)
    imp_LEFT = self.plot_imp_LEFT.imshow(self.img_no_data)
    imp_LEFT.format_cursor_data = lambda e: ""
    self.plot_imp_LEFT.set_axis_off()
    # add img to figures, plot, and remove axes
    self.plot_trial_LEFT = self.fig_trial_LEFT.add_subplot(111)
    trial_LEFT = self.plot_trial_LEFT.imshow(self.img_no_data)
    trial_LEFT.format_cursor_data = lambda e: ""
    self.plot_trial_LEFT.set_axis_off()
    # add img to figures, plot, and remove axes
    self.plot_imp_RIGHT = self.fig_imp_RIGHT.add_subplot(111)
    imp_RIGHT = self.plot_imp_RIGHT.imshow(self.img_no_data)
    imp_RIGHT.format_cursor_data = lambda e: ""
    self.plot_imp_RIGHT.set_axis_off()
    # add img to figures, plot, and remove axes
    self.plot_trial_RIGHT = self.fig_trial_RIGHT.add_subplot(111)
    trial_RIGHT = self.plot_trial_RIGHT.imshow(self.img_no_data)
    trial_RIGHT.format_cursor_data = lambda e: ""
    self.plot_trial_RIGHT.set_axis_off()

    # draw canvas
    self.canvas_imp_LEFT.draw()
    self.canvas_trial_LEFT.draw()
    self.canvas_imp_RIGHT.draw()
    self.canvas_trial_RIGHT.draw()

    self.btn_add_sac_LEFT.configure(state="disabled")
    self.btn_add_sac_RIGHT.configure(state="disabled")
    self.bth_upd_sac_LEFT.configure(state="disabled")
    self.bth_upd_sac_RIGHT.configure(state="disabled")

def plot_no_data(self):
    # adding the subplot and plotting
    # but first import No_Test_Found.png
    self.img_no_data = Image.open("Pictures/No_Data_Found.png")
    self.btn_export.configure(state="disabled")
    # plot No_Test_Found.png on first canvas
    self.fig_imp_LEFT.clear()
    self.fig_trial_LEFT.clear()
    self.fig_imp_RIGHT.clear()
    self.fig_trial_RIGHT.clear()
    # make figure fully fit canvas
    self.fig_imp_LEFT.set_tight_layout(True)
    self.fig_trial_LEFT.set_tight_layout(True)
    self.fig_imp_RIGHT.set_tight_layout(True)
    self.fig_trial_RIGHT.set_tight_layout(True)
    # add img to figures, plot, and remove show of coordinates and remove axes
    self.plot_imp_LEFT = self.fig_imp_LEFT.add_subplot(111)
    imp_LEFT = self.plot_imp_LEFT.imshow(self.img_no_data)
    imp_LEFT.format_cursor_data = lambda e: ""
    self.plot_imp_LEFT.set_axis_off()
    # add img to figures, plot, and remove show of coordinates and remove axes
    self.plot_trial_LEFT = self.fig_trial_LEFT.add_subplot(111)
    trial_LEFT= self.plot_trial_LEFT.imshow(self.img_no_data)
    trial_LEFT.format_cursor_data = lambda e: ""
    self.plot_trial_LEFT.set_axis_off()
    # add img to figures, plot, and remove show of coordinates and remove axes
    self.plot_imp_RIGHT = self.fig_imp_RIGHT.add_subplot(111)
    imp_RIGHT = self.plot_imp_RIGHT.imshow(self.img_no_data)
    imp_RIGHT.format_cursor_data = lambda e: ""
    self.plot_imp_RIGHT.set_axis_off()
    # add img to figures, plot, and remove show of coordinates and remove axes
    self.plot_trial_RIGHT = self.fig_trial_RIGHT.add_subplot(111)
    trial_RIGHT = self.plot_trial_RIGHT.imshow(self.img_no_data)
    trial_RIGHT.format_cursor_data = lambda e: ""
    self.plot_trial_RIGHT.set_axis_off()
    # draw canvas
    self.canvas_imp_LEFT.draw()
    self.canvas_trial_LEFT.draw()
    self.canvas_imp_RIGHT.draw()
    self.canvas_trial_RIGHT.draw()

def plot_no_impulse_LEFT(self):
    # adding the subplot and plotting
    # but first import No_Test_Found.png
    self.img_no_data = Image.open("Pictures/No_Impulse_Found.png")

    # plot No_Test_Found.png on first canvas
    self.fig_imp_LEFT.clear()
    self.fig_trial_LEFT.clear()
    # make figure fully fit canvas
    self.fig_imp_LEFT.set_tight_layout(True)
    self.fig_trial_LEFT.set_tight_layout(True)
    # add img to figures, plot, and remove axes
    self.plot_imp_LEFT = self.fig_imp_LEFT.add_subplot(111)
    imp_LEFT = self.plot_imp_LEFT.imshow(self.img_no_data)
    imp_LEFT.format_cursor_data = lambda e: ""
    self.plot_imp_LEFT.set_axis_off()
    # add img to figures, plot, and remove axes
    self.plot_trial_LEFT = self.fig_trial_LEFT.add_subplot(111)
    trial_LEFT = self.plot_trial_LEFT.imshow(self.img_no_data)
    trial_LEFT.format_cursor_data = lambda e: ""
    self.plot_trial_LEFT.set_axis_off()
    # draw canvas
    self.canvas_imp_LEFT.draw()
    self.canvas_trial_LEFT.draw()

    self.btn_add_sac_LEFT.configure(state="disabled")
    self.bth_upd_sac_LEFT.configure(state="disabled")

def plot_no_impulse_RIGHT(self):
    # adding the subplot and plotting
    # but first import No_Test_Found.png
    self.img_no_data = Image.open("Pictures/No_Impulse_Found.png")

    # plot No_Test_Found.png on first canvas
    self.fig_imp_RIGHT.clear()
    self.fig_trial_RIGHT.clear()    # make figure fully fit canvas
    self.fig_imp_RIGHT.set_tight_layout(True)
    self.fig_trial_RIGHT.set_tight_layout(True)
    # add img to figures, plot, and remove axes
    self.plot_imp_RIGHT = self.fig_imp_RIGHT.add_subplot(111)
    imp_RIGHT = self.plot_imp_RIGHT.imshow(self.img_no_data)
    imp_RIGHT.format_cursor_data = lambda e: ""
    self.plot_imp_RIGHT.set_axis_off()
    # add img to figures, plot, and remove axes
    self.plot_trial_RIGHT = self.fig_trial_RIGHT.add_subplot(111)
    trial_RIGHT = self.plot_trial_RIGHT.imshow(self.img_no_data)
    trial_RIGHT.format_cursor_data = lambda e: ""
    self.plot_trial_RIGHT.set_axis_off()
    # draw canvas
    self.canvas_imp_RIGHT.draw()
    self.canvas_trial_RIGHT.draw()

    self.btn_add_sac_RIGHT.configure(state="disabled")
    self.bth_upd_sac_RIGHT.configure(state="disabled")

def plot_graphs_imp_LEFT(self):
    # update canvases to draw new impulse graph for LEFT
    self.canvas_imp_LEFT.draw()

def plot_graphs_trial_LEFT(self):
    # update canvases to draw new trial graph for LEFT
    self.canvas_trial_LEFT.draw()

def plot_graphs_imp_RIGHT(self):
    # update canvases to draw new impulse graph for RIGHT
    self.canvas_imp_RIGHT.draw()

def plot_graphs_trial_RIGHT(self):
    # update canvases to draw new trial graph for RIGHT
    self.canvas_trial_RIGHT.draw()

def update_btn_LEFT(self):
    # but first check if onsets and offsets are within scopes of impulse timing
    for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():

        if self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get() < 0:
            self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].set(0)

        elif self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get() > \
                round(self.time_LEFT[self.name_order][self.imp_order_LEFT][-1]):
            self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].\
                set(round(self.time_LEFT[self.name_order][self.imp_order_LEFT][-1]) - 1)


        if self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get() < 0:
            self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].set(1)

        elif self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get() > \
                round(self.time_LEFT[self.name_order][self.imp_order_LEFT][-1]):
            self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].\
                set(round(self.time_LEFT[self.name_order][self.imp_order_LEFT][-1]))

    # update manually calcualated gain
    self.gain_manual_LEFT[self.name_order][self.imp_order_LEFT] = \
        analysis.calculate_gain(self.traces_LEFT[self.name_order][self.imp_order_LEFT][0],
                                self.traces_LEFT[self.name_order][self.imp_order_LEFT][1],
                                self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][0],
                                self.var_head_timing_LEFT[self.name_order][self.imp_order_LEFT][1],
                                self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT],
                                self.fps[self.name_order])

    # update saccade peaks
    # self.sacc_peak_LEFT[self.name_order][self.imp_order_LEFT] = [analysis.saccade_peak(self.traces_LEFT[self.name_order][self.imp_order_LEFT][1],
    #                                                                                    self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
    #                                                                                    self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(),
    #                                                                                    self.fps[self.name_order])
    #                                                              for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys()]

    self.sacc_peak_LEFT[self.name_order][self.imp_order_LEFT] = {}

    for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                self.sacc_peak_LEFT[self.name_order][self.imp_order_LEFT][key] = analysis.saccade_peak(self.traces_LEFT[self.name_order][self.imp_order_LEFT][1],
                                                                                                       self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                                                                                       self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(),
                                                                                                       self.fps[self.name_order])

    # update PR-scores
    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_LEFT[self.name_order] = analysis.calculate_PR1_peak(self.sacc_peak_LEFT[self.name_order],
                                                                 self.var_imp_take_auto_LEFT[self.name_order],
                                                                 self.var_imp_take_LEFT[self.name_order]
                                                                 )
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_LEFT[self.name_order] = analysis.calculate_PR2_peak(self.sacc_peak_LEFT[self.name_order],
                                                                 self.var_imp_take_auto_LEFT[self.name_order],
                                                                 self.var_imp_take_LEFT[self.name_order])
    # global
    self.PR_global_LEFT[self.name_order] = analysis.calculate_PR_global(self.PR1_LEFT[self.name_order][0],
                                                                        self.PR2_LEFT[self.name_order][0],
                                                                        self.PR1_LEFT[self.name_order][1],
                                                                        self.PR2_LEFT[self.name_order][1])
    ## redraw plots
    graph_figures_imp_LEFT(self)
    plot_graphs_imp_LEFT(self)
    graph_figures_trial_LEFT(self)
    plot_graphs_trial_LEFT(self)

def update_next_prev_LEFT(self, impulse):
    # update manually calcualated gain
    self.gain_manual_LEFT[self.name_order][impulse] = \
        analysis.calculate_gain(self.traces_LEFT[self.name_order][impulse][0],
                                self.traces_LEFT[self.name_order][impulse][1],
                                self.var_head_timing_LEFT[self.name_order][impulse][0],
                                self.var_head_timing_LEFT[self.name_order][impulse][1],
                                self.var_onset_offset_LEFT[self.name_order][impulse],
                                self.fps[self.name_order])

    # update saccade peaks
    # self.sacc_peak_LEFT[self.name_order][impulse] = [analysis.saccade_peak(self.traces_LEFT[self.name_order][impulse][1],
    #                                                                                    self.var_onset_offset_LEFT[self.name_order][impulse][key][0].get(),
    #                                                                                    self.var_onset_offset_LEFT[self.name_order][impulse][key][1].get(),
    #                                                                                    self.fps[self.name_order])
    #                                                              for key in self.var_onset_offset_LEFT[self.name_order][impulse].keys()]
    self.sacc_peak_LEFT[self.name_order][impulse] = {}

    for key in self.var_onset_offset_LEFT[self.name_order][impulse].keys():
        self.sacc_peak_LEFT[self.name_order][impulse][key] = analysis.saccade_peak(
            self.traces_LEFT[self.name_order][impulse][1],
            self.var_onset_offset_LEFT[self.name_order][impulse][key][0].get(),
            self.var_onset_offset_LEFT[self.name_order][impulse][key][1].get(),
            self.fps[self.name_order])

    # update PR-scores
    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_LEFT[self.name_order] = analysis.calculate_PR1_peak(self.sacc_peak_LEFT[self.name_order],
                                                                 self.var_imp_take_auto_LEFT[self.name_order],
                                                                 self.var_imp_take_LEFT[self.name_order]
                                                                 )
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_LEFT[self.name_order] = analysis.calculate_PR2_peak(self.sacc_peak_LEFT[self.name_order],
                                                                 self.var_imp_take_auto_LEFT[self.name_order],
                                                                 self.var_imp_take_LEFT[self.name_order])
    # global
    self.PR_global_LEFT[self.name_order] = analysis.calculate_PR_global(self.PR1_LEFT[self.name_order][0],
                                                                        self.PR2_LEFT[self.name_order][0],
                                                                        self.PR1_LEFT[self.name_order][1],
                                                                        self.PR2_LEFT[self.name_order][1])

    ## redraw plots
    graph_figures_trial_LEFT(self)
    plot_graphs_trial_LEFT(self)

def update_btn_RIGHT(self):
    # but first check if onsets and offsets are within scopes of impulse timing
    for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():

        if self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get() < 0:
            self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].set(0)

        elif self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get() > \
                round(self.time_RIGHT[self.name_order][self.imp_order_RIGHT][-1]):
            self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].\
                set(round(self.time_RIGHT[self.name_order][self.imp_order_RIGHT][-1]) - 1)


        if self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get() < 0:
            self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].set(1)

        elif self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get() > \
                round(self.time_RIGHT[self.name_order][self.imp_order_RIGHT][-1]):
            self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].\
                set(round(self.time_RIGHT[self.name_order][self.imp_order_RIGHT][-1]))

    # update manually calcualated gain
    self.gain_manual_RIGHT[self.name_order][self.imp_order_RIGHT] = \
        analysis.calculate_gain(self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][0],
                                self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][1],
                                self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][0],
                                self.var_head_timing_RIGHT[self.name_order][self.imp_order_RIGHT][1],
                                self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT],
                                self.fps[self.name_order])

    # update saccade peaks
    self.sacc_peak_RIGHT[self.name_order][self.imp_order_RIGHT] = {}

    for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                self.sacc_peak_RIGHT[self.name_order][self.imp_order_RIGHT][key] = analysis.saccade_peak(self.traces_RIGHT[self.name_order][self.imp_order_RIGHT][1],
                                                                                                         self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get(),
                                                                                                         self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get(),
                                                                                                         self.fps[self.name_order])

    # update PR-scores
    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_RIGHT[self.name_order] = analysis.calculate_PR1_peak(self.sacc_peak_RIGHT[self.name_order],
                                                                  self.var_imp_take_auto_RIGHT[self.name_order],
                                                                  self.var_imp_take_RIGHT[self.name_order]
                                                                  )
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_RIGHT[self.name_order] = analysis.calculate_PR2_peak(self.sacc_peak_RIGHT[self.name_order],
                                                                  self.var_imp_take_auto_RIGHT[self.name_order],
                                                                  self.var_imp_take_RIGHT[self.name_order])
    # global
    self.PR_global_RIGHT[self.name_order] = analysis.calculate_PR_global(self.PR1_RIGHT[self.name_order][0],
                                                                        self.PR2_RIGHT[self.name_order][0],
                                                                        self.PR1_RIGHT[self.name_order][1],
                                                                        self.PR2_RIGHT[self.name_order][1])

    # redraw plots
    graph_figures_imp_RIGHT(self)
    plot_graphs_imp_RIGHT(self)
    graph_figures_trial_RIGHT(self)
    plot_graphs_trial_RIGHT(self)

def update_next_prev_RIGHT(self, impulse):
    # update manually calcualated gain
    self.gain_manual_RIGHT[self.name_order][impulse] = \
        analysis.calculate_gain(self.traces_RIGHT[self.name_order][impulse][0],
                                self.traces_RIGHT[self.name_order][impulse][1],
                                self.var_head_timing_RIGHT[self.name_order][impulse][0],
                                self.var_head_timing_RIGHT[self.name_order][impulse][1],
                                self.var_onset_offset_RIGHT[self.name_order][impulse],
                                self.fps[self.name_order])

    # update saccade peaks
    self.sacc_peak_RIGHT[self.name_order][impulse] = {}

    for key in self.var_onset_offset_RIGHT[self.name_order][impulse].keys():
        self.sacc_peak_RIGHT[self.name_order][impulse][key] = analysis.saccade_peak(
            self.traces_RIGHT[self.name_order][impulse][1],
            self.var_onset_offset_RIGHT[self.name_order][impulse][key][0].get(),
            self.var_onset_offset_RIGHT[self.name_order][impulse][key][1].get(),
            self.fps[self.name_order])

    # update PR-scores
    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_RIGHT[self.name_order] = analysis.calculate_PR1_peak(self.sacc_peak_RIGHT[self.name_order],
                                                                  self.var_imp_take_auto_RIGHT[self.name_order],
                                                                  self.var_imp_take_RIGHT[self.name_order]
                                                                  )
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_RIGHT[self.name_order] = analysis.calculate_PR2_peak(self.sacc_peak_RIGHT[self.name_order],
                                                                  self.var_imp_take_auto_RIGHT[self.name_order],
                                                                  self.var_imp_take_RIGHT[self.name_order])
    # global
    self.PR_global_RIGHT[self.name_order] = analysis.calculate_PR_global(self.PR1_RIGHT[self.name_order][0],
                                                                         self.PR2_RIGHT[self.name_order][0],
                                                                         self.PR1_RIGHT[self.name_order][1],
                                                                         self.PR2_RIGHT[self.name_order][1])
    # redraw plots
    graph_figures_trial_RIGHT(self)
    plot_graphs_trial_RIGHT(self)

def plotting(self, who):
    # after importing (or closing nw_window) present impulse of first subject on Canvas_Imp and all trial on Canvas_Trial
    # who presents who called function: Import button or buttons switching subjects - 0
    #                                   buttons switching impulses - 1
    # if trial exists
    if self.sessions[self.name_order] != []:
        # if impulse for LEFT exist
        if self.traces_LEFT[self.name_order] != []:
            # bind these figures with graphs of impulses
            # check who called plotting()
            # if Import button ot buttons switching subjects
            #if who == 0:
            graph_figures_imp_LEFT(self)
            graph_figures_trial_LEFT(self)
            # plot embedded graphs
            plot_graphs_imp_LEFT(self)
            plot_graphs_trial_LEFT(self)
            # if buttons switching impulses
            #else:
            graph_figures_imp_LEFT(self)
            plot_graphs_imp_LEFT(self)
            # write impulse number of first subject into label4_LEFT
            self.label4_LEFT.configure(text="Impulse {} from {}".format(self.imp_order_LEFT + 1,
                                                                        len(self.traces_LEFT[self.name_order])))
        else:
            plot_no_impulse_LEFT(self)
            # write impulse number of first subject into label4_RIGHT
            self.label4_LEFT.configure(text="Impulse - from -")

        # if impulse for RIGHT exist
        if self.traces_RIGHT[self.name_order] != []:
            # check who called plotting()
            # if Import button ot buttons switching subjects
            #if who == 0:
            graph_figures_imp_RIGHT(self)
            graph_figures_trial_RIGHT(self)
            # plot embedded graphs
            plot_graphs_imp_RIGHT(self)
            plot_graphs_trial_RIGHT(self)
            # if buttons switching impulses
            #else:
            graph_figures_imp_RIGHT(self)
            plot_graphs_imp_RIGHT(self)
            # write impulse number of first subject into label4_RIGHT
            self.label4_RIGHT.configure(text="Impulse {} from {}".format(self.imp_order_RIGHT + 1,
                                                                         len(self.traces_RIGHT[self.name_order])))
        else:
            plot_no_impulse_RIGHT(self)
            # write impulse number of first subject into label4_RIGHT
            self.label4_RIGHT.configure(text="Impulse - from -")
    # if subject has no data present "no data" picture on both canvases for LEFT and RIGHT
    else:
        plot_no_trial(self)