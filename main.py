import tkinter as tk
import tkinter.ttk as ttk
from os import listdir
from tkinter import filedialog
from os.path import isfile, join
import xlsxwriter
import re
import datetime
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
# custom modules
import workWithXML
import analysis
import nw_sessions
import get_traces
import plotting

#import sys
#sys.coinit_flags = 2  # COINIT_APARTMENTTHREADED


class GUI_vHIT:
    def __init__(self, master):
        ## global parameters
        # set main window as class object
        self.master = master
        # set scale coefficient of screen width and height to create canvas and plot resized img on it
        self.scale_img = 25
        # get window size
        self.width = master.winfo_screenwidth()
        self.height = master.winfo_screenheight()
        # set geometry of master window
        self.master.geometry("{}x{}".format(int(self.width / 100 * 83), int(self.height / 100 * 90)))
        ## create widgets
        # create parent notebook with two tabs for Otometrics and EyeSeeCam
        # create notebook
        #Oto_Eye_nb = ttk.Notebook(master)
        # create tabs
        #self.Frame_Oto = tk.Frame(Oto_Eye_nb)
        #self.Frame_Eye = tk.Frame(Oto_Eye_nb)
        # pack tabs onto notebook
        #self.Frame_Oto.pack(fill="both", expand=True)
        #self.Frame_Eye.pack(fill="both", expand=True)
        #Oto_Eye_nb.add(self.Frame_Oto, text="Otometrics")
        #Oto_Eye_nb.add(self.Frame_Eye, text="EyeSeeCam")
        # pack notebook on master window
        #Oto_Eye_nb.pack(fill='both', expand=1)

        ## now work on each tab separately
        ## start with Otometrics tab (Frame_Oto)
        # create buttons to import files and start/Save/Export
        self.Frame_Oto = tk.Frame(self.master)
        self.Frame_Oto.pack(fill="both", expand=True)
        self.frm_imp_exp = tk.Frame(self.Frame_Oto)
        self.frm_imp_exp.grid(row=0, column=0, stick="nw")
        self.Button_Path = tk.Button(self.frm_imp_exp, width=10, text="Import files", borderwidth=3, command=self.Import)
        self.btn_export = tk.Button(self.frm_imp_exp, width=10, text="Export", borderwidth=3, command=self.Export, state="disabled")
        # pack these buttons using grid scheme
        self.Button_Path.pack(side="left")
        self.btn_export.pack(side="left")

        # create label and entry to set head peak threshold used to remove dab traces
        # but first create frame to pack this label and entry
        self.frame_head_sac_th = tk.Frame(self.Frame_Oto)
        # pack this frame
        self.frame_head_sac_th.grid(row=3, column=0, sticky="nw")
        # and a variable to store entry value
        self.var_head_th = tk.IntVar(value=120)
        # create label
        self.lbl_head_th1 = tk.Label(self.frame_head_sac_th, text=" Head peak threshold ")
        # create entry to set value of threshold
        self.ent_head_th = tk.Entry(self.frame_head_sac_th, width=3, textvariable=self.var_head_th)
        # another label
        self.lbl_head_th2 = tk.Label(self.frame_head_sac_th, text="deg/sec")
        # pack this label and 2 entries
        self.lbl_head_th1.pack(side="left")
        self.ent_head_th.pack(side="left")
        self.lbl_head_th2.pack(side="left")

        # create label and entry to set saccade peak threshold used to remove and pack them into frame_head_th
        # variable to store entry value
        self.var_sac_th = tk.IntVar(value=60)
        # create label
        self.lbl_sac_th1 = tk.Label(self.frame_head_sac_th, text=" Saccade peak threshold ")
        # create entry to set value of threshold
        self.ent_sac_th = tk.Entry(self.frame_head_sac_th, width=3, textvariable=self.var_sac_th)
        # another label
        self.lbl_sac_th2 = tk.Label(self.frame_head_sac_th, text="deg/sec")
        # pack this label and 2 entries
        self.lbl_sac_th1.pack(side="left")
        self.ent_sac_th.pack(side="left")
        self.lbl_sac_th2.pack(side="left")

        # create option to choose between HIMP and SHIMP
        # for this create tk variable which value can be traced and 0 means HIMP, 1 - SHIMP
        self.HIMPorSHIMP = tk.IntVar()
        self.HIMPorSHIMP.set(0)
        # create new frame to pack radiobuttons for choosing between HIMP and SHIMP
        self.Frame_RadBut = tk.Frame(self.Frame_Oto)
        # pack this frame below Import Files button
        self.Frame_RadBut.grid(row=1, column=0, stick="nw")
        # create two radiobuttons belonging to Frame_RadBut
        self.RadBut_HIMP = tk.Radiobutton(self.Frame_RadBut, text="HIMP", variable=self.HIMPorSHIMP, value=0)
        self.RadBut_SHIMP = tk.Radiobutton(self.Frame_RadBut, text="SHIMP", variable=self.HIMPorSHIMP, value=1)
        # pack radiobuttons into the Frame_RadBut
        # but before create and pack label
        label_test = tk.Label(self.Frame_RadBut, text="Test: ")
        label_test.pack(side="left")
        self.RadBut_HIMP.pack(side="left")
        self.RadBut_SHIMP.pack(side="left")

        # create option to choose between Lateral, RALP, or LARP test planes
        # for this create tk variable which value can be traced and 0 means Lateral, 1 - RALP, 2 - LARP
        self.plane = tk.IntVar()
        self.plane.set(0)
        # create new frame to pack radiobuttons for choosing between Lateral, RALP, or LARP
        self.Frame_plane = tk.Frame(self.Frame_Oto)
        # pack this frame below Import Files button
        self.Frame_plane.grid(row=2, column=0, stick="nw")
        # create two radiobuttons belonging to Frame_RadBut
        self.RadBut_Lateral = tk.Radiobutton(self.Frame_RadBut, text="Lateral", variable=self.plane, value=0)
        self.RadBut_RALP = tk.Radiobutton(self.Frame_RadBut, text="RALP", variable=self.plane, value=1)
        self.RadBut_LARP = tk.Radiobutton(self.Frame_RadBut, text="LARP", variable=self.plane, value=2)
        # pack radiobuttons into the Frame_plane
        # but before create and pack label
        label_plane = tk.Label(self.Frame_RadBut, text="Plane: ")
        label_plane.pack(side="left")
        self.RadBut_Lateral.pack(side="left")
        self.RadBut_RALP.pack(side="left")
        self.RadBut_LARP.pack(side="left")

        # create 4 frames to pack 4 canvases for presenting
        # both individual impulse and trial for both LEFT and RIGHT
        plotting.create_canvas_frames(self)
        # also create 4 figures which will store either graphs or images
        plotting.create_figures(self)
        # set fornt for figure labels
        plotting.set_font(self)

        # create label with patient name and order and two buttons to switch between subjects on Frame_Oto
        # but first create frame to pack these buttons and label into it
        self.Frame3 = tk.Frame(self.Frame_Oto)
        # pack this frame3 into Frame_Oto
        self.Frame3.grid(row=0, column=1, stick="nw")
        # create label to show patient name and number
        self.label3 = tk.Label(self.Frame3)
        self.label3.pack()
        # create buttons which switch subjects and pack them into Frame_Oto
        self.btn_next = tk.Button(self.Frame3, width=10, text=">>", borderwidth=3, command=self.fun_btn_next, state="disabled")
        self.btn_prev = tk.Button(self.Frame3, width=10, text="<<", borderwidth=3, command=self.fun_btn_prev, state="disabled")
        self.btn_prev.pack(side="left")
        self.btn_next.pack(side="left")

        # create label with impulse number and two buttons to switch between impulses for LEFT and RIGHT
        # for LEFT
        # but first create frame to pack these buttons and label into it
        self.Frame4_LEFT = tk.Frame(self.Frame_Oto)
        # pack this frame3 into Frame_Oto
        self.Frame4_LEFT.grid(row=4, column=0, stick="nw")
        # create label to show impulse number
        self.label4_LEFT = tk.Label(self.Frame4_LEFT)
        self.label4_LEFT.pack()
        # create buttons which switch impulses and pack them into Frame4_LEFT
        self.btn_next_imp_LEFT = tk.Button(self.Frame4_LEFT, width=10, text=">>", borderwidth=3, command=self.fun_btn_next_imp_LEFT, state="disabled")
        self.btn_prev_imp_LEFT = tk.Button(self.Frame4_LEFT, width=10, text="<<", borderwidth=3, command=self.fun_btn_prev_imp_LEFT, state="disabled")
        self.btn_prev_imp_LEFT.pack(side="left")
        self.btn_next_imp_LEFT.pack(side="left")

        # for RIGHT
        # but first create frame to pack these buttons and label into it
        self.Frame4_RIGHT = tk.Frame(self.Frame_Oto)
        # pack this frame3 into Frame_Oto
        self.Frame4_RIGHT.grid(row=4, column=2, stick="nw")
        # create label to show impulse number
        self.label4_RIGHT = tk.Label(self.Frame4_RIGHT)
        self.label4_RIGHT.pack()
        # create buttons which switch subjects and pack them into Frame4_RIGHT
        self.btn_next_imp_RIGHT = tk.Button(self.Frame4_RIGHT, width=10, text=">>", borderwidth=3, command=self.fun_btn_next_imp_RIGHT, state="disabled")
        self.btn_prev_imp_RIGHT = tk.Button(self.Frame4_RIGHT, width=10, text="<<", borderwidth=3, command=self.fun_btn_prev_imp_RIGHT, state="disabled")
        self.btn_prev_imp_RIGHT.pack(side="left")
        self.btn_next_imp_RIGHT.pack(side="left")

        # create 4 frames to pack 4 canvases
        plotting.create_canvas_frames(self)
        # create 4 figure to bind graphs with canvases
        plotting.create_figures(self)
        # create 4 canvases and bind them with 4 figures
        # create 4 frames to pack 4 canvases
        self.canvas_imp_LEFT = FigureCanvasTkAgg(self.fig_imp_LEFT, master=self.frame_canvas_imp_LEFT)
        # draw canvas
        self.canvas_imp_LEFT.draw()
        # creating Matplotlib toolbar
        self.toolbar_imp_LEFT = NavigationToolbar2Tk(self.canvas_imp_LEFT, self.frame_canvas_imp_LEFT)
        # update toolbar
        self.toolbar_imp_LEFT.update()
        # pack canvas into frame_canvas
        self.canvas_imp_LEFT.get_tk_widget().pack(side="left")

        # create canvas for individual impulse for RIGHT
        self.canvas_imp_RIGHT = FigureCanvasTkAgg(self.fig_imp_RIGHT, master=self.frame_canvas_imp_RIGHT)
        # draw canvas
        self.canvas_imp_RIGHT.draw()
        # creating Matplotlib toolbar
        self.toolbar_imp_RIGHT = NavigationToolbar2Tk(self.canvas_imp_RIGHT, self.frame_canvas_imp_RIGHT)
        # update toolbar
        self.toolbar_imp_RIGHT.update()
        # pack canvas into frame_canvas
        self.canvas_imp_RIGHT.get_tk_widget().pack(side="left")

        # create canvas for trial for LEFT
        self.canvas_trial_LEFT = FigureCanvasTkAgg(self.fig_trial_LEFT, master=self.frame_canvas_trial_LEFT)
        # draw canvas
        self.canvas_trial_LEFT.draw()
        # pack canvas into frame_canvas
        self.canvas_imp_LEFT.get_tk_widget().pack(side="left")
        # creating Matplotlib toolbar
        self.toolbar_trial_LEFT = NavigationToolbar2Tk(self.canvas_trial_LEFT, self.frame_canvas_trial_LEFT)
        # update toolbar
        self.toolbar_trial_LEFT.update()
        # pack canvas into frame_canvas
        self.canvas_trial_LEFT.get_tk_widget().pack(side="left")

        # create canvas for trial for RIGHT
        self.canvas_trial_RIGHT = FigureCanvasTkAgg(self.fig_trial_RIGHT, master=self.frame_canvas_trial_RIGHT)
        # draw canvas
        self.canvas_trial_RIGHT.draw()
        # pack canvas into frame_canvas
        self.canvas_imp_LEFT.get_tk_widget().pack(side="left")
        # creating Matplotlib toolbar
        self.toolbar_trial_RIGHT = NavigationToolbar2Tk(self.canvas_trial_RIGHT, self.frame_canvas_trial_RIGHT)
        # update toolbar
        self.toolbar_trial_RIGHT.update()
        # pack canvas into frame_canvas
        self.canvas_trial_RIGHT.get_tk_widget().pack(side="left")

        # create two frames to present saccade timings
        self.frame_sac_LEFT = tk.Frame(self.Frame_Oto)
        self.frame_sac_RIGHT = tk.Frame(self.Frame_Oto)
        # pack these frames
        self.frame_sac_LEFT.grid(row=5, column=1, rowspan=3, stick="nw")
        self.frame_sac_RIGHT.grid(row=5, column=3, rowspan=3, stick="nw")
        # create button to add saccade
        self.btn_add_sac_LEFT = tk.Button(self.frame_sac_LEFT, width=3, text="Add", borderwidth=3, state="disabled",
                                          command=lambda: self.fun_btn_add_sac_LEFT(0, 10, key=len(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys())+1, who=0))
        self.btn_add_sac_RIGHT = tk.Button(self.frame_sac_RIGHT, width=3, text="Add", borderwidth=3, state="disabled",
                                           command=lambda: self.fun_btn_add_sac_RIGHT(0, 10, key=len(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys())+1, who=0))
        # create buttons to update canvas when saccade is manually changes
        self.bth_upd_sac_LEFT = tk.Button(self.frame_sac_LEFT, width=5, text="Update", borderwidth=2, state="disabled",
                                          command=lambda: plotting.update_btn_LEFT(self))
        self.bth_upd_sac_RIGHT = tk.Button(self.frame_sac_RIGHT, width=5, text="Update", borderwidth=2, state="disabled",
                                           command=lambda: plotting.update_btn_RIGHT(self))
        # pack these buttons
        self.btn_add_sac_LEFT.grid(row=0, column=0, stick="nw")
        self.btn_add_sac_RIGHT.grid(row=0, column=0, stick="nw")
        self.bth_upd_sac_LEFT.grid(row=0, column=1, columnspan=3, stick="nw")
        self.bth_upd_sac_RIGHT.grid(row=0, column=1, columnspan=3, stick="nw")
        # create empty dictionary storing form (labels and entries above) of added saccades
        self.form_added_sac_LEFT = {}
        self.form_added_sac_RIGHT = {}
        # create variable showing number of added saccade
        self.num_added_sac_LEFT = 0
        self.num_added_sac_RIGHT = 0
        # counter for tracing how may time import button was pressed
        self.cnt_import = 0

        # create checkboxes to choose whether take or remove impulse
        # created to be destroyed when import data first time
        # needed to allow checkboxes be destroyed each time when change subject/impulse or import ne data
        self.ckb_imp_take_LEFT = tk.Checkbutton(self.Frame_Oto)
        self.ckb_imp_take_RIGHT = tk.Checkbutton(self.Frame_Oto)

        # put vesrion and my email
        frm_version_mail = tk.Frame(self.Frame_Oto)
        frm_version_mail.grid(row=8, column=0, columnspan=2, sticky="w")
        lbl_version = tk.Label(frm_version_mail, text="By StArK v1.1.4 ")
        lbl_mail = tk.Label(frm_version_mail, text="dstark2048@gmail.com")
        lbl_version.pack(side="left")
        lbl_mail.pack(side="left")

    def Import(self):
        # but first clean data from previous imports if variables exist
        if self.cnt_import > 0:
            del self.mypath, self.files, self.tree_xml, self.pref, self.names, self.sessions, self.sessions_ind,\
                self.sessions_number, self.name_order, self.imp_order_LEFT, self.imp_order_RIGHT, self.var_imp_take_LEFT,\
                self.var_imp_take_RIGHT, self.gain_device_LEFT, self.gain_device_RIGHT, self.gain_manual_LEFT,\
                self.gain_manual_RIGHT, self.traces_LEFT, self.traces_RIGHT, self.colors, self.var_head_peak_LEFT,\
                self.var_head_peak_RIGHT, self.var_head_timing_LEFT, self.var_head_timing_RIGHT, self.var_head_3sd_LEFT,\
                self.var_head_3sd_RIGHT, self.var_imp_take_auto_LEFT, self.var_imp_take_auto_RIGHT, self.saccades_LEFT,\
                self.saccades_RIGHT, self.var_onset_offset_LEFT, self.var_onset_offset_RIGHT, self.sacc_peak_LEFT,\
                self.sacc_peak_RIGHT, self.var_form_order_LEFT, self.var_form_order_RIGHT, self.num_added_sac_LEFT,\
                self.num_added_sac_RIGHT, self.PR1_LEFT, self.PR1_RIGHT, self.PR2_LEFT, self.PR2_RIGHT, self.PR_global_LEFT,\
                self.PR_global_RIGHT

            for key in self.form_added_sac_LEFT.keys():
                self.form_added_sac_LEFT[key][0].destroy()
                self.form_added_sac_LEFT[key][1].destroy()
                self.form_added_sac_LEFT[key][2].destroy()
                self.form_added_sac_LEFT[key][3].destroy()
                self.form_added_sac_LEFT[key][4].destroy()

            for key in self.form_added_sac_RIGHT.keys():
                self.form_added_sac_RIGHT[key][0].destroy()
                self.form_added_sac_RIGHT[key][1].destroy()
                self.form_added_sac_RIGHT[key][2].destroy()
                self.form_added_sac_RIGHT[key][3].destroy()
                self.form_added_sac_RIGHT[key][4].destroy()

            self.form_added_sac_LEFT = {}
            self.form_added_sac_RIGHT = {}

            self.label3.configure(text="Subject's name: - with # -/-")
            self.label4_LEFT.configure(text="Impulse - from -")
            self.label4_RIGHT.configure(text="Impulse - from -")

        ## function reads all files, takes only *.xml, extracts names, all sessions, ask to choose one session by opening a new window
        # deactivate buttons on Frame_oto which switch subjects (will be activated if Import is successful)
        self.btn_next.configure(state="disabled")
        self.btn_prev.configure(state="disabled")
        self.btn_next_imp_LEFT.configure(state="disabled")
        self.btn_prev_imp_LEFT.configure(state="disabled")
        self.btn_next_imp_RIGHT.configure(state="disabled")
        self.btn_prev_imp_RIGHT.configure(state="disabled")
        # destroy checkboxes which store info whether to take or remove impulse
        self.ckb_imp_take_LEFT.destroy()
        self.ckb_imp_take_RIGHT.destroy()
        self.btn_add_sac_LEFT.configure(state="disabled")
        self.btn_add_sac_RIGHT.configure(state="disabled")
        self.bth_upd_sac_LEFT.configure(state="disabled")
        self.bth_upd_sac_RIGHT.configure(state="disabled")
        # create 4 canvases and bind them with 4 figures
        # open files
        self.mypath = filedialog.askdirectory()
        # if button open was pressed (if CANCEL - nothing happens)
        if self.mypath != '':
            # get list of names of the files (including extension) and take only ones having ".xml" at the end
            self.files = [f for f in listdir(self.mypath) if isfile(join(self.mypath, f)) and re.search(r"\.xml$", f)]
            # check if at least one file was read
            if len(self.files) != 0:
                # setting cnt_import to 1 means files were existed
                self.cnt_import = 1
                self.btn_add_sac_LEFT.configure(state="active")
                self.btn_add_sac_RIGHT.configure(state="active")
                self.bth_upd_sac_LEFT.configure(state="active")
                self.bth_upd_sac_RIGHT.configure(state="active")
                # function to sort file names stored as str, return integer part of a file name
                def get_int_from_str(file_name):
                    return int(''.join([i for i in file_name if i.isdigit()]))

                # sort file names based on an integer key extracted using the function: get_int_from_str
                self.files.sort(key=get_int_from_str)

                # read all xml files as tuple of roots of elements called tree (each tree is a xml file)
                self.tree_xml = tuple(workWithXML.parse_xml(self.mypath, file).getroot() for file in self.files)
                # read prefix for each XML file from namespace xmlns = {URI}.
                # Used for adding to atributes as ({prefix}attrib) while searching for it using root.find, root.findall, or root.iter functions
                self.pref = tuple(workWithXML.get_pref(root) for root in self.tree_xml)

                # create text value depending on the radiobutton value
                if self.HIMPorSHIMP.get() == 0:
                    self.HIMPorSHIMP_text = "VW_HITest"
                else:
                    self.HIMPorSHIMP_text = "VW_SHIMPTest"

                # create text value depending on the radiobutton value
                if self.plane.get() == 0:
                    if self.HIMPorSHIMP_text == "VW_SHIMPTest":
                        self.plane_text = "SHIMP Lateral"
                    else:
                        self.plane_text = "Lateral"
                elif self.plane.get() == 1:
                    if self.HIMPorSHIMP_text == "VW_SHIMPTest":
                        self.plane_text = "SHIMP RALP"
                    else:
                        self.plane_text = "RALP"
                else:
                    if self.HIMPorSHIMP_text == "VW_SHIMPTest":
                        self.plane_text = "SHIMP LARP"
                    else:
                        self.plane_text = "LARP"

                # get first names
                self.names = tuple(workWithXML.get_names(self.tree_xml[i], self.pref[i])
                                   for i in range(len(self.pref)))
                # write name and number of first subject into label3
                self.label3.configure(text="Subject's name: {} with # {}/{}".format(self.names[0], 1, len(self.names)))
                # activate buttons on Frame_oto which switch subjects
                self.btn_next.configure(state="active")
                self.btn_next.configure(state="active")
                # get all sessions per subject using test type (HIMPorSHIMP) and plane (Lateral, RALP, LARP)
                self.sessions = [workWithXML.get_sessions(self.tree_xml[i], self.pref[i], self.HIMPorSHIMP_text, self.plane_text)
                                 for i in range(len(self.pref))]

                # count how many sessions
                self.sessions_number = tuple(map(lambda x: len(x), self.sessions))
                # get indexes of sessions > 1
                self.sessions_ind = tuple(i for i in range(len(self.sessions_number)) if self.sessions_number[i] > 1)
                # if at least one subject has more than one sessions open a new window
                if len(self.sessions_ind) > 0:
                    #self.create_nw_session(self)
                    nw_sessions.create_nw_session(self)
                # if no, extract impulses, mark bad traces, do calculations, and plot
                else:
                    # but first reset name_order to start from first subject
                    self.name_order = 0
                    # and imp_order to start from first impulse
                    self.imp_order_LEFT = 0
                    self.imp_order_RIGHT = 0
                    # get average frame rate per session
                    get_traces.get_framerate(self)
                    # extract impulses to LEFT and RIGHT per subject
                    # first variable get_impulses_traces_time returns list of impulses
                    # each element of list is list where first variable is impulses to LEFT, second variable - to RIGHT
                    # also returns eye and head traces and times for them
                    get_traces.get_impulses_traces_time(self)
                    # create variables storing whether remove (0) or take (1) imp for all subjects
                    self.var_imp_take_LEFT = tuple([tk.IntVar(value=1) for j in range(len(self.traces_LEFT[i]))]
                                                   for i in range(len(self.sessions)))
                    self.var_imp_take_RIGHT = tuple([tk.IntVar(value=1) for j in range(len(self.traces_RIGHT[i]))]
                                                    for i in range(len(self.sessions)))
                    # get gain per impulse calculated by device

                    self.gain_device_LEFT = (tuple(tuple(workWithXML.get_gain_device(self.impulses[i][0][j], self.pref[i])
                                                         for j in range(len(self.impulses[i][0])))
                                                   for i in range(len(self.sessions))))
                    self.gain_device_RIGHT = (tuple(tuple(workWithXML.get_gain_device(self.impulses[i][1][j], self.pref[i])
                                                          for j in range(len(self.impulses[i][1])))
                                                    for i in range(len(self.sessions))))

                    # check if LEFT impulses exist to activate switching button
                    if self.traces_LEFT[self.name_order] != []:
                       self.btn_next_imp_LEFT.configure(state="active")
                       # also call function to create checkbox and assign corresponding imp to it
                       self.fun_imp_take_ckb_create_LEFT()

                    # check if RIGHT impulses exist to activate switching button
                    if self.traces_RIGHT[self.name_order] != []:
                       self.btn_next_imp_RIGHT.configure(state="active")
                       # call function to create checkbox and assign corresponding variable to it
                       self.fun_imp_take_ckb_create_RIGHT()

                    # create list of colors to paint saccades and corresponding buttons removing them
                    self.colors = ["#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE"]

                    ## for LEFT
                    # get head peaks
                    # variable structure: [subject][impulse][index, value]
                    self.var_head_peak_LEFT = tuple([analysis.head_peak(self.traces_LEFT[i][j][0])
                                                     for j in range(len(self.traces_LEFT[i]))]
                                                    for i in range(len(self.sessions)))

                    # get head onset and offset
                    # variable structure: [subject][impulse][onset, offset]
                    self.var_head_timing_LEFT = tuple([[analysis.head_onset(self.traces_LEFT[i][j][0], self.fps[i]),
                                                        analysis.head_offset(self.traces_LEFT[i][j][0])]
                                                       for j in range(len(self.traces_LEFT[i]))]
                                                      for i in range(len(self.sessions)))

                    # check different criteria to remove impulse
                    # variable structure of var_imp_take_auto: [subject][impulse][head peak < threshold,
                    #                                                             head peak out of +-3sd of trial
                    #                                                               (see var_head_3sd_LEFT and head_out_3sd),
                    #                                                             no head offset,
                    #                                                             head bounce > 50% of head peak]
                    # for LEFT
                    # but first get interval calculated as:
                    # 1) mean of the interval prior 80msec to and 120msec after peak head calculated for each impulse
                    # 2) MEAN of all such means of all impulses
                    # 3) std of all such means of all impulses
                    # 4) interval [MEAN - 3std, MEAN + 3std]
                    self.var_head_3sd_LEFT = tuple(analysis.head_peak_3sdt(self.traces_LEFT[i], self.fps[i])
                                                   for i in range(len(self.sessions)))
                    # check each criterion
                    self.var_imp_take_auto_LEFT = tuple(
                        [[analysis.head_less_th(self.traces_LEFT[i][j][0], self.var_head_th.get()),
                          analysis.head_out_3sd(self.traces_LEFT[i][j][0], self.var_head_3sd_LEFT[i],
                                                self.fps[i]),
                          analysis.head_no_offset(self.var_head_timing_LEFT[i][j][1]),
                          analysis.head_bounce(self.traces_LEFT[i][j][0])]
                         for j in range(len(self.traces_LEFT[i]))]
                        for i in range(len(self.sessions)))

                    # extract saccades
                    self.saccades_LEFT = tuple(
                        [analysis.saccades(self.traces_LEFT[i][j][1], self.var_head_timing_LEFT[i][j][0],
                                           #+round(60/((1/self.fps[i])*1000)),
                                           self.fps[i], self.var_sac_th.get())
                         for j in range(len(self.traces_LEFT[i]))]
                        for i in range(len(self.sessions)))

                    # add form to edit or remove extracted saccades
                    # but first create variable to track onsets and offsets
                    # these variables are created each time when impulse or subject are changed
                    self.var_onset_offset_LEFT = [[{} for j in range(len(self.traces_LEFT[i]))]
                                                    for i in range(len(self.sessions))]
                    # copy saccades to editable variable
                    for i in range(len(self.saccades_LEFT)):
                        for j in range(len(self.saccades_LEFT[i])):
                            for k in range(len(self.saccades_LEFT[i][j])):
                                self.var_onset_offset_LEFT[i][j][k] = [tk.IntVar(value=round(
                                    self.saccades_LEFT[i][j][k][0] * (1 / self.fps[i]) * 1000)),
                                                                       tk.IntVar(value=round(
                                                                           self.saccades_LEFT[i][j][k][1] * (
                                                                                       1 / self.fps[i]) * 1000))]

                    # get positions of saccades peaks
                    self.sacc_peak_LEFT = [[{} for j in range(len(self.traces_LEFT[i]))]
                                           for i in range(len(self.sessions))]

                    for i in range(len(self.saccades_LEFT)):
                        for j in range(len(self.saccades_LEFT[i])):
                            for key in self.var_onset_offset_LEFT[i][j].keys():
                                self.sacc_peak_LEFT[i][j][key] = analysis.saccade_peak(self.traces_LEFT[i][j][1],
                                                                                       self.var_onset_offset_LEFT[i][j][
                                                                                           key][0].get(),
                                                                                       self.var_onset_offset_LEFT[i][j][
                                                                                           key][1].get(),
                                                                                       self.fps[i])

                    # also create variable to track if these forms created first time fot given subject and impulse
                    # variable equal to 0 means first time, 1 - other times
                    self.var_form_order_LEFT = [[0 for j in range(len(self.traces_LEFT[i]))]
                                                for i in range(len(self.sessions))]
                    # create variable to be used as key in dictionary where edited saccades are stored
                    self.num_added_sac_LEFT = [[0 for j in range(len(self.traces_LEFT[i]))]
                                                for i in range(len(self.sessions))]

                    if self.traces_LEFT[self.name_order] != []:
                        # create editable form for each saccade of selected subject and impulse
                        for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                            self.fun_btn_add_sac_LEFT(
                                self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(),
                                key=key, who=1)
                        # reset var_form_form_order for first subject first impulse
                        self.var_form_order_LEFT[self.name_order][self.imp_order_LEFT] = 1

                    ## for RIGHT
                    # get head peaks
                    # variable structure: [subject][impulse][index, value]
                    self.var_head_peak_RIGHT = tuple([analysis.head_peak(self.traces_RIGHT[i][j][0])
                                                      for j in range(len(self.traces_RIGHT[i]))]
                                                     for i in range(len(self.sessions)))

                    # get head onset and offset
                    # variable structure: [subject][impulse][onset, offset]
                    self.var_head_timing_RIGHT = tuple(
                        [[analysis.head_onset(self.traces_RIGHT[i][j][0], self.fps[i]),
                          analysis.head_offset(self.traces_RIGHT[i][j][0])]
                         for j in range(len(self.traces_RIGHT[i]))]
                        for i in range(len(self.sessions)))

                    # check different criteria to remove impulse
                    # variable structure of var_imp_take_auto: [subject][impulse][head peak < threshold,
                    #                                                             head peak out of +-3sd of trial
                    #                                                               (see var_head_3sd_LEFT and head_out_3sd),
                    #                                                             no head offset,
                    #                                                             head bounce > 50% of head peak]
                    # for LEFT
                    # but first get interval calculated as:
                    # 1) mean of the interval prior 80msec to and 120msec after peak head calculated for each impulse
                    # 2) MEAN of all such means of all impulses
                    # 3) std of all such means of all impulses
                    # 4) interval [MEAN - 3std, MEAN + 3std]
                    self.var_head_3sd_RIGHT = tuple(analysis.head_peak_3sdt(self.traces_RIGHT[i], self.fps[i])
                                                    for i in range(len(self.sessions)))
                    # check each criterion
                    self.var_imp_take_auto_RIGHT = tuple(
                        [[analysis.head_less_th(self.traces_RIGHT[i][j][0], self.var_head_th.get()),
                          analysis.head_out_3sd(self.traces_RIGHT[i][j][0], self.var_head_3sd_RIGHT[i],
                                                self.fps[i]),
                          analysis.head_no_offset(self.var_head_timing_RIGHT[i][j][1]),
                          analysis.head_bounce(self.traces_RIGHT[i][j][0])]
                         for j in range(len(self.traces_RIGHT[i]))]
                        for i in range(len(self.sessions)))

                    # extract saccades
                    self.saccades_RIGHT = tuple(
                        [analysis.saccades(self.traces_RIGHT[i][j][1], self.var_head_timing_RIGHT[i][j][0],
                                           #+round(60/((1/self.fps[i])*1000)),
                                           self.fps[i], self.var_sac_th.get())
                         for j in range(len(self.traces_RIGHT[i]))]
                        for i in range(len(self.sessions)))

                    # add form to edit or remove extracted saccades
                    # but first create variable to track onsets and offsets
                    # these variables are created each time when impulse or subject are changed
                    self.var_onset_offset_RIGHT = [[{} for j in range(len(self.traces_RIGHT[i]))]
                                                   for i in range(len(self.sessions))]
                    # copy saccades to editable variable
                    for i in range(len(self.saccades_RIGHT)):
                        for j in range(len(self.saccades_RIGHT[i])):
                            for k in range(len(self.saccades_RIGHT[i][j])):
                                self.var_onset_offset_RIGHT[i][j][k] = [tk.IntVar(value=round(
                                    self.saccades_RIGHT[i][j][k][0] * (1 / self.fps[i]) * 1000)),
                                                                        tk.IntVar(value=round(
                                                                            self.saccades_RIGHT[i][j][k][1] * (
                                                                                        1 / self.fps[i]) * 1000))]

                    # get positions of saccades peaks
                    self.sacc_peak_RIGHT = [[{} for j in range(len(self.traces_RIGHT[i]))]
                                            for i in range(len(self.sessions))]

                    for i in range(len(self.saccades_RIGHT)):
                        for j in range(len(self.saccades_RIGHT[i])):
                            for key in self.var_onset_offset_RIGHT[i][j].keys():
                                self.sacc_peak_RIGHT[i][j][key] = analysis.saccade_peak(self.traces_RIGHT[i][j][1],
                                                                                        self.var_onset_offset_RIGHT[i][
                                                                                            j][key][0].get(),
                                                                                        self.var_onset_offset_RIGHT[i][
                                                                                            j][key][1].get(),
                                                                                        self.fps[i])

                    # also create variable to track if these forms created first time fot given subject and impulse
                    # variable equal to 0 means first time, 1 - other times
                    self.var_form_order_RIGHT = [[0 for j in range(len(self.traces_RIGHT[i]))]
                                                 for i in range(len(self.sessions))]
                    # create variable to be used as key in dictionary where edited saccades are stored
                    self.num_added_sac_RIGHT = [[0 for j in range(len(self.traces_RIGHT[i]))]
                                                    for i in range(len(self.sessions))]

                    if self.traces_RIGHT[self.name_order] != []:
                        # create editable form for each saccade of selected subject and impulse
                        for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                            self.fun_btn_add_sac_RIGHT(
                                self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0],
                                self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1],
                                key=key, who=1)
                        # reset var_form_form_order for first subject first impulse
                        self.var_form_order_RIGHT[self.name_order][self.imp_order_RIGHT] = 1

                    # calculate gain for each desaccaded impulse
                    # for LEFT
                    self.gain_manual_LEFT = [
                        [analysis.calculate_gain(self.traces_LEFT[i][j][0], self.traces_LEFT[i][j][1],
                                                 self.var_head_timing_LEFT[i][j][0],
                                                 self.var_head_timing_LEFT[i][j][1],
                                                 self.var_onset_offset_LEFT[i][j], self.fps[i])
                         for j in range(len(self.traces_LEFT[i]))]
                        for i in range(len(self.sessions))]

                    # for RIGHT
                    self.gain_manual_RIGHT = [
                        [analysis.calculate_gain(self.traces_RIGHT[i][j][0], self.traces_RIGHT[i][j][1],
                                                 self.var_head_timing_RIGHT[i][j][0],
                                                 self.var_head_timing_RIGHT[i][j][1],
                                                 self.var_onset_offset_RIGHT[i][j], self.fps[i])
                         for j in range(len(self.traces_RIGHT[i]))]
                        for i in range(len(self.sessions))]

                    ## get PR scores for
                    # first order saccades with following structure: [subject][PR1, number (how many) of first order saccades]
                    self.PR1_LEFT = [analysis.calculate_PR1_peak(self.sacc_peak_LEFT[i],
                                                                 self.var_imp_take_auto_LEFT[i],
                                                                 self.var_imp_take_LEFT[i])
                                     for i in range(len(self.sessions))]
                    # second order saccades with following structure: [subject][PR2, number of second order saccades]
                    self.PR2_LEFT = [analysis.calculate_PR2_peak(self.sacc_peak_LEFT[i],
                                                                 self.var_imp_take_auto_LEFT[i],
                                                                 self.var_imp_take_LEFT[i])
                                     for i in range(len(self.sessions))]
                    # global
                    self.PR_global_LEFT = [analysis.calculate_PR_global(self.PR1_LEFT[i][0], self.PR2_LEFT[i][0],
                                                                        self.PR1_LEFT[i][1], self.PR2_LEFT[i][1])
                                           for i in range(len(self.sessions))]

                    # first order saccades with following structure: [subject][PR1, number of first order saccades]
                    self.PR1_RIGHT = [analysis.calculate_PR1_peak(self.sacc_peak_RIGHT[i],
                                                                  self.var_imp_take_auto_RIGHT[i],
                                                                  self.var_imp_take_RIGHT[i])
                                      for i in range(len(self.sessions))]
                    # second order saccades with following structure: [subject][PR2, number of second order saccades]
                    self.PR2_RIGHT = [analysis.calculate_PR2_peak(self.sacc_peak_RIGHT[i],
                                                                  self.var_imp_take_auto_RIGHT[i],
                                                                  self.var_imp_take_RIGHT[i])
                                      for i in range(len(self.sessions))]
                    # global
                    self.PR_global_RIGHT = [analysis.calculate_PR_global(self.PR1_RIGHT[i][0], self.PR2_RIGHT[i][0],
                                                                         self.PR1_RIGHT[i][1], self.PR2_RIGHT[i][1])
                                            for i in range(len(self.sessions))]

                    # start plotting
                    # plotting function automatically detect empty sessions
                    plotting.plotting(self, who=0)
                    # activate button Export
                    self.btn_export.configure(state="active")
            # if no files were found, draw img_no_data on all canvases
            else:
                # if files were not found setting cnt_import to 0 means nothing to remove
                self.cnt_import = 0
                # create checkboxes to choose whether take or remove impulse
                # created to be destroyed when import data first time
                # needed to allow checkboxes be destroyed each time when change subject/impulse or import ne data
                self.ckb_imp_take_LEFT = tk.Checkbutton(self.Frame_Oto)
                self.ckb_imp_take_RIGHT = tk.Checkbutton(self.Frame_Oto)
                plotting.plot_no_data(self)



    def fun_btn_next(self):
        self.btn_add_sac_LEFT.configure(state="active")
        self.btn_add_sac_RIGHT.configure(state="active")
        self.bth_upd_sac_LEFT.configure(state="active")
        self.bth_upd_sac_RIGHT.configure(state="active")
        # switch to next subject
        # if selected subject is NOT last in list
        if self.name_order < (len(self.sessions) - 1):
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_LEFT.destroy()
            self.ckb_imp_take_RIGHT.destroy()
            # increment iterator name_order by 1
            self.name_order = self.name_order + 1
            # if this is last subject deactivate btn_next
            if self.name_order == (len(self.sessions) - 1):
                self.btn_next.configure(state="disabled")
            # if this is second subject activate btn_prev
            if self.name_order == 1:
                self.btn_prev.configure(state="active")
            # reconfigure text of label3 showing subject's name and number
            self.label3.configure(text="Subject's name: {} with # {}/{}".format(self.names[self.name_order], self.name_order + 1, len(self.names)))
            # activate buttons switching to next impulses and deactivate ones switching to previous impulses
            # also reset imp_order_LEFT and imp_order_RIGHT to 0
            # check if LEFT impulses exist to activate switching button
            self.imp_order_LEFT = 0
            if self.traces_LEFT[self.name_order] != []:
                self.btn_next_imp_LEFT.configure(state="active")
                self.btn_prev_imp_LEFT.configure(state="disabled")
                # also call function to create checkbox and assign corresponding imp to it
                self.fun_imp_take_ckb_create_LEFT()
            # if no deactivate all buttons
            else:
                self.btn_next_imp_LEFT.configure(state="disabled")
                self.btn_prev_imp_LEFT.configure(state="disabled")

            # check if RIGHT impulses exist to activate switching button
            self.imp_order_RIGHT = 0
            if self.traces_RIGHT[self.name_order] != []:
                self.btn_next_imp_RIGHT.configure(state="active")
                self.btn_prev_imp_RIGHT.configure(state="disabled")
                # also call function to create checkbox and assign corresponding imp to it
                self.fun_imp_take_ckb_create_RIGHT()
            # if no deactivate all buttons
            else:
                self.btn_next_imp_RIGHT.configure(state="disabled")
                self.btn_prev_imp_RIGHT.configure(state="disabled")

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            for widget in self.frame_sac_LEFT.winfo_children()[2:]:
                widget.destroy()
            for widget in self.frame_sac_RIGHT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_LEFT = {}
            self.form_added_sac_RIGHT = {}
            # check if these form were already created
            # if not use saccades
            if self.traces_LEFT[self.name_order] != []:
                for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                    self.fun_btn_add_sac_LEFT(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                                self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(), key=key, who=1)

            if self.traces_RIGHT[self.name_order] != []:
                for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                    self.fun_btn_add_sac_RIGHT(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get(),
                                                self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.plotting(self, who=0)

    def fun_btn_prev(self):
        self.btn_add_sac_LEFT.configure(state="active")
        self.btn_add_sac_RIGHT.configure(state="active")
        self.bth_upd_sac_LEFT.configure(state="active")
        self.bth_upd_sac_RIGHT.configure(state="active")
        # switch to previous subject
        # if selected subject is NOT first in list
        if self.name_order > 0:
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_LEFT.destroy()
            self.ckb_imp_take_RIGHT.destroy()
            # decrement iterator name_order by 1
            self.name_order = self.name_order - 1
            # if this is first subject deactivate btn_prev
            if self.name_order == 0:
                self.btn_prev.configure(state="disabled")
            # if this is one before last subject activate btn_next
            if self.name_order == (len(self.sessions) - 2):
                self.btn_next.configure(state="active")
            # reconfigure text of label3 showing subject's name and number
            self.label3.configure(text="Subject's name: {} with # {}/{}".format(self.names[self.name_order], self.name_order + 1, len(self.names)))
            # activate buttons switching to next impulses and deactivate ones switching to previous impulses
            # also reset imp_order_LEFT and imp_order_RIGHT to 0
            # check if LEFT impulses exist to activate switching button
            self.imp_order_LEFT = 0
            if self.traces_LEFT[self.name_order] != []:
                self.btn_next_imp_LEFT.configure(state="active")
                self.btn_prev_imp_LEFT.configure(state="disabled")
                # also call function to create checkbox and assign corresponding imp to it
                self.fun_imp_take_ckb_create_LEFT()
            # if no deactivate all buttons
            else:
                self.btn_next_imp_LEFT.configure(state="disabled")
                self.btn_prev_imp_LEFT.configure(state="disabled")

            # check if RIGHT impulses exist to activate switching button
            self.imp_order_RIGHT = 0
            if self.traces_RIGHT[self.name_order] != []:
                self.btn_next_imp_RIGHT.configure(state="active")
                self.btn_prev_imp_RIGHT.configure(state="disabled")
                # also call function to create checkbox and assign corresponding imp to it
                self.fun_imp_take_ckb_create_RIGHT()
            # if no deactivate all buttons
            else:
                self.btn_next_imp_RIGHT.configure(state="disabled")
                self.btn_prev_imp_RIGHT.configure(state="disabled")

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            for widget in self.frame_sac_LEFT.winfo_children()[2:]:
                widget.destroy()
            for widget in self.frame_sac_RIGHT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_LEFT = {}
            self.form_added_sac_RIGHT = {}

            if self.traces_LEFT[self.name_order] != []:
                for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                    self.fun_btn_add_sac_LEFT(
                        self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                        self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(), key=key, who=1)

            if self.traces_RIGHT[self.name_order] != []:
                for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                    self.fun_btn_add_sac_RIGHT(
                        self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get(),
                        self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.plotting(self, who=0)
            # activate button Export
            self.btn_export.configure(state="active")

    def fun_btn_next_imp_LEFT(self):
        # switch to next impulse
        # if selected subject is NOT last in list
        if self.imp_order_LEFT < (len(self.traces_LEFT[self.name_order]) - 1):
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_LEFT.destroy()
            # increment iterator name_order by 1
            self.imp_order_LEFT = self.imp_order_LEFT + 1
            # if this is last impulse deactivate btn_next_imp_LEFT
            if self.imp_order_LEFT == (len(self.traces_LEFT[self.name_order]) - 1):
                self.btn_next_imp_LEFT.configure(state="disabled")
            # if this is second subject activate btn_prev_imp_LEFT
            if self.imp_order_LEFT == 1:
                self.btn_prev_imp_LEFT.configure(state="active")
            # reconfigure text of label3 showing impulse number
            self.label4_LEFT.configure(text="Impulse {} from {}".format(self.imp_order_LEFT + 1,
                                                                        len(self.traces_LEFT[self.name_order])))
            # also call function to create checkbox and assign corresponding imp to it
            # but first destroy previous checkbox
            self.ckb_imp_take_LEFT.destroy()
            self.fun_imp_take_ckb_create_LEFT()

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            # but first delete all previous forms saving buttons "Add" and "Update"
            for widget in self.frame_sac_LEFT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_LEFT = {}
            # check if these form were already created
            # if not use saccades
            if self.traces_LEFT[self.name_order] != []:
                for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                    self.fun_btn_add_sac_LEFT(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                              self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.update_next_prev_LEFT(self, self.imp_order_LEFT - 1)
            plotting.plotting(self, who=1)

    def fun_btn_prev_imp_LEFT(self):
        # switch to previous impulse
        # if selected subject is NOT first in list
        if self.imp_order_LEFT > 0:
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_LEFT.destroy()
            # decrement iterator name_order by 1
            self.imp_order_LEFT = self.imp_order_LEFT - 1
            # if this is first subject deactivate btn_prev_imp_LEFT
            if self.imp_order_LEFT == 0:
                self.btn_prev_imp_LEFT.configure(state="disabled")
            # if this is one before last subject activate btn_next_imp_LEFT
            if self.imp_order_LEFT == (len(self.traces_LEFT[self.name_order]) - 2):
                self.btn_next_imp_LEFT.configure(state="active")
            # reconfigure text of label3 showing impulse number
            self.label4_LEFT.configure(text="Impulse {} from {}".format(self.imp_order_LEFT + 1,
                                                                        len(self.traces_LEFT[self.name_order])))
            # also call function to create checkbox and assign corresponding imp to it
            # but first destroy previous checkbox
            self.ckb_imp_take_LEFT.destroy()
            self.fun_imp_take_ckb_create_LEFT()

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            # but first delete all previous forms saving buttons "Add" and "Update"
            for widget in self.frame_sac_LEFT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_LEFT = {}
            # check if these form were already created
            # if not use saccades
            if self.traces_LEFT[self.name_order] != []:
                for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
                    self.fun_btn_add_sac_LEFT(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                              self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.update_next_prev_LEFT(self, self.imp_order_LEFT + 1)
            plotting.plotting(self, who=1)

    def fun_btn_next_imp_RIGHT(self):
        # switch to next impulse
        # if selected subject is NOT last in list
        if self.imp_order_RIGHT < (len(self.traces_RIGHT[self.name_order]) - 1):
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_RIGHT.destroy()
            # increment iterator name_order by 1
            self.imp_order_RIGHT = self.imp_order_RIGHT + 1
            # if this is last impulse deactivate btn_next_imp_RIGHT
            if self.imp_order_RIGHT == (len(self.traces_RIGHT[self.name_order]) - 1):
                self.btn_next_imp_RIGHT.configure(state="disabled")
            # if this is second subject activate btn_prev_imp_RIGHT
            if self.imp_order_RIGHT == 1:
                self.btn_prev_imp_RIGHT.configure(state="active")
            # reconfigure text of label3 showing impulse number
            self.label4_RIGHT.configure(text="Impulse {} from {}".format(self.imp_order_RIGHT + 1,
                                                                         len(self.traces_RIGHT[self.name_order])))
            # also call function to create checkbox and assign corresponding imp to it
            # but first destroy previous checkbox
            self.ckb_imp_take_RIGHT.destroy()
            self.fun_imp_take_ckb_create_RIGHT()

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            # but first delete all previous forms saving buttons "Add" and "Update"
            for widget in self.frame_sac_RIGHT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_RIGHT = {}
            # check if these form were already created
            # if not use saccades
            if self.traces_RIGHT[self.name_order] != []:
                for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                    self.fun_btn_add_sac_RIGHT(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get(),
                                               self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.update_next_prev_RIGHT(self, self.imp_order_RIGHT - 1)
            plotting.plotting(self, who=1)

    def fun_btn_prev_imp_RIGHT(self):
        # switch to previous impulse
        # if selected subject is NOT first in list
        if self.imp_order_RIGHT > 0:
            # destroy checkboxes which store info whether to take or remove impulse
            self.ckb_imp_take_RIGHT.destroy()
            # decrement iterator name_order by 1
            self.imp_order_RIGHT = self.imp_order_RIGHT - 1
            # if this is first subject deactivate btn_prev_imp_RIGHT
            if self.imp_order_RIGHT == 0:
                self.btn_prev_imp_RIGHT.configure(state="disabled")
            # if this is one before last subject activate btn_next_imp_RIGHT
            if self.imp_order_RIGHT == (len(self.traces_RIGHT[self.name_order]) - 2):
                self.btn_next_imp_RIGHT.configure(state="active")
            # reconfigure text of label3 showing impulse number
            self.label4_RIGHT.configure(text="Impulse {} from {}".format(self.imp_order_RIGHT + 1,
                                                                         len(self.traces_RIGHT[self.name_order])))
            # also call function to create checkbox and assign corresponding imp to it
            # but first destroy previous checkbox
            self.ckb_imp_take_RIGHT.destroy()
            self.fun_imp_take_ckb_create_RIGHT()

            # create editable form for each saccade of selected subject and impulse
            # but first delete all previous forms
            for widget in self.frame_sac_RIGHT.winfo_children()[2:]:
                widget.destroy()
            # create empty dictionary storing form (labels and entries above) of added saccades
            self.form_added_sac_RIGHT = {}
            # check if these form were already created
            # if not use saccades
            if self.traces_RIGHT[self.name_order] != []:
                for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
                    self.fun_btn_add_sac_RIGHT(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0].get(),
                                               self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1].get(), key=key, who=1)

            # plot traces
            plotting.update_next_prev_RIGHT(self, self.imp_order_RIGHT + 1)
            plotting.plotting(self, who=1)

    def fun_btn_add_sac_LEFT(self, onset, offset, key, who):
        # create input form on frame_sac_LEFT for adding new saccade
        # create button to remove added sacade, label to point at saccade onset and offset,
        # and text windows to edit saccade onset and offset
        # and add new item to variables for retracing onsets and offsets
        # but check who called function: if who = 0 - btn_add_saccade
        #                                if who = 1 - button import or buttons to switch subject or impulse
        if who == 0:
            # increase key by 1 to add new saccade
            last_key = list(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys())
            if last_key != []:
                key = last_key[-1] + 1
            else:
                key = 0

            self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key] = [tk.IntVar(value=round(onset*(1/self.fps[self.name_order])*1000)),
                                                                                     tk.IntVar(value=round(offset*(1/self.fps[self.name_order])*1000))]
        # self.var_offset.append(tk.IntVar(value=onset))
        #
        self.form_added_sac_LEFT[key] = [tk.Button(self.frame_sac_LEFT, width=1, text="X", borderwidth=3, fg=self.colors[key],
                                                   command=lambda: self.fun_del_added_sac_LEFT(key)),
                                         tk.Label(self.frame_sac_LEFT, text="onset"),
                                         tk.Entry(self.frame_sac_LEFT, width=3,
                                                  textvariable=self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0]),
                                         tk.Label(self.frame_sac_LEFT, text="offset"),
                                         tk.Entry(self.frame_sac_LEFT, width=3,
                                                  textvariable=self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1])]

        # pack all items in row
        self.form_added_sac_LEFT[key][0].grid(row=key+1, column=0)
        self.form_added_sac_LEFT[key][1].grid(row=key+1, column=1)
        self.form_added_sac_LEFT[key][2].grid(row=key+1, column=2)
        self.form_added_sac_LEFT[key][3].grid(row=key+1, column=3)
        self.form_added_sac_LEFT[key][4].grid(row=key+1, column=4)
        # increment num_added_sac_LEFT by 1
        self.num_added_sac_LEFT[self.name_order][self.imp_order_LEFT] += 1

    def fun_btn_add_sac_RIGHT(self, onset, offset, key, who):
        # create input form on frame_sac_LEFT for adding new saccade
        # create button to remove added sacade, label to point at saccade onset and offset,
        # and text windows to edit saccade onset and offset
        # but first reserve number of added saccade to be able to delete it
        #key = self.num_added_sac_RIGHT[self.name_order][self.imp_order_RIGHT]
        # and add new item to variables for retracing onsets and offsets
        # but check who called function: if who = 0 - btn_add_saccade
        #                                if who = 1 - button import or buttons to switch subject or impulse
        if who == 0:
            # increase key by 1 to add new saccade
            last_key = list(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys())
            if last_key != []:
                key = last_key[-1] + 1
            else:
                key = 0

            self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key] = [tk.IntVar(value=round(onset*(1/self.fps[self.name_order])*1000)),
                                                                                       tk.IntVar(value=round(offset*(1/self.fps[self.name_order])*1000))]
        # and add new item to variables for retracing onsets and offsets
        self.form_added_sac_RIGHT[key] = [tk.Button(self.frame_sac_RIGHT, width=1, text="X", borderwidth=3, fg=self.colors[key],
                                                    command=lambda: self.fun_del_added_sac_RIGHT(key)),
                                          tk.Label(self.frame_sac_RIGHT, text="Onset"),
                                          tk.Entry(self.frame_sac_RIGHT, width=3,
                                                   textvariable=self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0]),
                                          tk.Label(self.frame_sac_RIGHT, text="offset"),
                                          tk.Entry(self.frame_sac_RIGHT, width=3,
                                                   textvariable=self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1])]

        # pack all items in row
        self.form_added_sac_RIGHT[key][0].grid(row=key + 1, column=0)
        self.form_added_sac_RIGHT[key][1].grid(row=key + 1, column=1)
        self.form_added_sac_RIGHT[key][2].grid(row=key + 1, column=2)
        self.form_added_sac_RIGHT[key][3].grid(row=key + 1, column=3)
        self.form_added_sac_RIGHT[key][4].grid(row=key + 1, column=4)
        # increment num_added_sac_LEFT by 1
        self.num_added_sac_RIGHT[self.name_order][self.imp_order_RIGHT] = self.num_added_sac_RIGHT[self.name_order][self.imp_order_RIGHT] + 1

    def fun_del_added_sac_LEFT(self, key):
        # delete form with key for added saccade from form_added_sac_LEFT
        self.form_added_sac_LEFT[key][0].destroy()
        self.form_added_sac_LEFT[key][1].destroy()
        self.form_added_sac_LEFT[key][2].destroy()
        self.form_added_sac_LEFT[key][3].destroy()
        self.form_added_sac_LEFT[key][4].destroy()
        del self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key]
        del self.form_added_sac_LEFT[key]

    def fun_del_added_sac_RIGHT(self, key):
        # delete form with key for added saccade from form_added_sac_LEFT
        self.form_added_sac_RIGHT[key][0].destroy()
        self.form_added_sac_RIGHT[key][1].destroy()
        self.form_added_sac_RIGHT[key][2].destroy()
        self.form_added_sac_RIGHT[key][3].destroy()
        self.form_added_sac_RIGHT[key][4].destroy()
        del self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key]
        del self.form_added_sac_RIGHT[key]

    def fun_imp_take_ckb_create_LEFT(self):
        # create checkbox to take or remove impulse
        # for LEFT
        self.ckb_imp_take_LEFT = tk.Checkbutton(self.Frame_Oto, text="Take impulse",
                                                variable=self.var_imp_take_LEFT[self.name_order][self.imp_order_LEFT])
        # pack checkbox
        self.ckb_imp_take_LEFT.grid(row=6, column=0, stick="w")

    def fun_imp_take_ckb_create_RIGHT(self):
        # create checkbox to take or remove impulse
        # for RIGHT
        self.ckb_imp_take_RIGHT = tk.Checkbutton(self.Frame_Oto, text="Take impulse",
                                                 variable=self.var_imp_take_RIGHT[self.name_order][
                                                     self.imp_order_RIGHT])
        # pack checkbox
        self.ckb_imp_take_RIGHT.grid(row=6, column=2, stick="w")

    def Export(self):
        # Create new Excel file and add worksheet.
        # first get current date and time to embed into file name
        time = datetime.datetime.now()
        workbook = xlsxwriter.Workbook(self.mypath+'/DB_{}_{}_{}__{}_{}.xlsx'.format(time.year, time.month, time.day, time.hour, time.minute))
        worksheet = workbook.add_worksheet()

        # Widen 15 columns to make text clearer.
        for i in range(21):
            worksheet.set_column(i, 0, 12)
        # Add a bold format to use to highlight cells.
        bold = workbook.add_format({'bold': True})

        # Write text.
        worksheet.write(0, 0, 'Subject')
        worksheet.write(0, 1, 'Test')
        worksheet.write(0, 2, 'Plane')
        worksheet.write(0, 3, 'N_impulses')
        worksheet.write(0, 4, 'Impulse')
        worksheet.write(0, 5, 'Side')
        worksheet.write(0, 6, 'Bounce>50%')
        worksheet.write(0, 7, 'Out_3sd')
        worksheet.write(0, 8, 'Head_peak<{}'.format(self.var_head_th.get()))
        worksheet.write(0, 9, 'No_head_offset')
        worksheet.write(0, 10, 'Manual_take')
        worksheet.write(0, 11, 'Head_onset')
        worksheet.write(0, 12, 'Head_offset')
        worksheet.write(0, 13, 'Head peak')
        worksheet.write(0, 14, 'Head peak_latency')
        worksheet.write(0, 15, 'Gain_Device')
        worksheet.write(0, 16, 'Gain_Manual')
        worksheet.write(0, 17, 'PR1')
        worksheet.write(0, 18, 'PR2')
        worksheet.write(0, 19, 'PR_global')
        worksheet.write(0, 20, 'Compensatory')
        worksheet.write(0, 21, 'Sacc_onset')
        worksheet.write(0, 22, 'Sacc_offset')
        worksheet.write(0, 23, 'Sacc_peak_latency')
        worksheet.write(0, 24, 'Sacc_peak_absolute')
        worksheet.write(0, 25, 'Sacc_peak_relative')

        cnt = 1
        for i in range(len(self.sessions)):
            for j in range(len(self.traces_LEFT[i])):
                if self.var_onset_offset_LEFT[i][j] == {}:
                    worksheet.write(cnt, 0, self.names[i])
                    worksheet.write(cnt, 1, self.HIMPorSHIMP_text)
                    worksheet.write(cnt, 2, self.plane_text)
                    worksheet.write(cnt, 3, len(self.traces_LEFT[i]))
                    worksheet.write(cnt, 4, j+1)
                    worksheet.write(cnt, 5, "LEFT")
                    worksheet.write(cnt, 6, bool(self.var_imp_take_auto_LEFT[i][j][3]))
                    worksheet.write(cnt, 7, bool(self.var_imp_take_auto_LEFT[i][j][1]))
                    worksheet.write(cnt, 8, bool(self.var_imp_take_auto_LEFT[i][j][0]))
                    worksheet.write(cnt, 9, bool(self.var_imp_take_auto_LEFT[i][j][2]))
                    worksheet.write(cnt, 10, bool(self.var_imp_take_LEFT[i][j].get()))

                    worksheet.write(cnt, 11, round(self.var_head_timing_LEFT[i][j][0]*(1/self.fps[i])*1000))
                    worksheet.write(cnt, 12, round(self.var_head_timing_LEFT[i][j][1]*(1/self.fps[i])*1000))

                    worksheet.write(cnt, 13, round(self.var_head_peak_LEFT[i][j][1]))
                    worksheet.write(cnt, 14, round(self.var_head_peak_LEFT[i][j][0] * (1/self.fps[i])*1000 - \
                                                   self.var_head_timing_LEFT[i][j][0] * (1/self.fps[i])*1000))
                    worksheet.write(cnt, 15, self.gain_device_LEFT[i][j])
                    worksheet.write(cnt, 16, self.gain_manual_LEFT[i][j])
                    worksheet.write(cnt, 17, self.PR1_LEFT[i][0])
                    worksheet.write(cnt, 18, self.PR2_LEFT[i][0])
                    worksheet.write(cnt, 19, self.PR_global_LEFT[i])
                    worksheet.write(cnt, 20, "")
                    worksheet.write(cnt, 21, "")
                    worksheet.write(cnt, 22, "")
                    worksheet.write(cnt, 23, "")
                    worksheet.write(cnt, 24, "")
                    worksheet.write(cnt, 25, "")
                    cnt += 1
                else:
                    for key in self.var_onset_offset_LEFT[i][j].keys():
                        worksheet.write(cnt, 0, self.names[i])
                        worksheet.write(cnt, 1, self.HIMPorSHIMP_text)
                        worksheet.write(cnt, 2, self.plane_text)
                        worksheet.write(cnt, 3, len(self.traces_LEFT[i]))
                        worksheet.write(cnt, 4, j+1)
                        worksheet.write(cnt, 5, "LEFT")
                        worksheet.write(cnt, 6, bool(self.var_imp_take_auto_LEFT[i][j][3]))
                        worksheet.write(cnt, 7, bool(self.var_imp_take_auto_LEFT[i][j][1]))
                        worksheet.write(cnt, 8, bool(self.var_imp_take_auto_LEFT[i][j][0]))
                        worksheet.write(cnt, 9, bool(self.var_imp_take_auto_LEFT[i][j][2]))
                        worksheet.write(cnt, 10, bool(self.var_imp_take_LEFT[i][j].get()))

                        worksheet.write(cnt, 11, round(self.var_head_timing_LEFT[i][j][0] * (1 / self.fps[i]) * 1000))
                        worksheet.write(cnt, 12, round(self.var_head_timing_LEFT[i][j][1] * (1 / self.fps[i]) * 1000))

                        worksheet.write(cnt, 13, round(self.var_head_peak_LEFT[i][j][1]))
                        worksheet.write(cnt, 14, round(self.var_head_peak_LEFT[i][j][0] * (1 / self.fps[i])*1000 - \
                                                       self.var_head_timing_LEFT[i][j][0] * (1/self.fps[i])*1000))
                        worksheet.write(cnt, 15, self.gain_device_LEFT[i][j])
                        worksheet.write(cnt, 16, self.gain_manual_LEFT[i][j])
                        worksheet.write(cnt, 17, self.PR1_LEFT[i][0])
                        worksheet.write(cnt, 18, self.PR2_LEFT[i][0])
                        worksheet.write(cnt, 19, self.PR_global_LEFT[i])
                        worksheet.write(cnt, 20, True if (self.traces_LEFT[i][j][1][round(self.sacc_peak_LEFT[i][j][key]*self.fps[i]/1000)] > 0 and \
                                                         self.HIMPorSHIMP_text == "VW_HITest") or \
                                                         (self.traces_LEFT[i][j][1][round(self.sacc_peak_LEFT[i][j][key]*self.fps[i]/1000)] < 0 and \
                                                          self.HIMPorSHIMP_text == "VW_SHIMPTest") \
                                                 else False)
                        worksheet.write(cnt, 21, round(self.var_onset_offset_LEFT[i][j][key][0].get() - self.var_head_timing_LEFT[i][j][0]*(1/self.fps[i])*1000))
                        worksheet.write(cnt, 22, round(self.var_onset_offset_LEFT[i][j][key][1].get() - self.var_head_timing_LEFT[i][j][0]*(1/self.fps[i])*1000))

                        worksheet.write(cnt, 23, self.sacc_peak_LEFT[i][j][key] - round(self.var_head_timing_LEFT[i][j][0]*(1/self.fps[i])*1000))

                        worksheet.write(cnt, 24, round(abs(self.traces_LEFT[i][j][1][round(self.sacc_peak_LEFT[i][j][key]*self.fps[i]/1000)])))
                        worksheet.write(cnt, 25, self.sacc_peak_LEFT[i][j][key]/round(self.var_head_peak_LEFT[i][j][1]))
                        cnt += 1

        for i in range(len(self.sessions)):
            for j in range(len(self.traces_RIGHT[i])):
                if self.var_onset_offset_RIGHT[i][j] == {}:
                    worksheet.write(cnt, 0, self.names[i])
                    worksheet.write(cnt, 1, self.HIMPorSHIMP_text)
                    worksheet.write(cnt, 2, self.plane_text)
                    worksheet.write(cnt, 3, len(self.traces_RIGHT[i]))
                    worksheet.write(cnt, 4, j+1)
                    worksheet.write(cnt, 5, "RIGHT")
                    worksheet.write(cnt, 6, bool(self.var_imp_take_auto_RIGHT[i][j][3]))
                    worksheet.write(cnt, 7, bool(self.var_imp_take_auto_RIGHT[i][j][1]))
                    worksheet.write(cnt, 8, bool(self.var_imp_take_auto_RIGHT[i][j][0]))
                    worksheet.write(cnt, 9, bool(self.var_imp_take_auto_RIGHT[i][j][2]))
                    worksheet.write(cnt, 10, bool(self.var_imp_take_RIGHT[i][j].get()))

                    worksheet.write(cnt, 11, round(self.var_head_timing_RIGHT[i][j][0] * (1 / self.fps[i]) * 1000))
                    worksheet.write(cnt, 12, round(self.var_head_timing_RIGHT[i][j][1] * (1 / self.fps[i]) * 1000))


                    worksheet.write(cnt, 13, round(self.var_head_peak_RIGHT[i][j][1]))
                    worksheet.write(cnt, 14, round(self.var_head_peak_RIGHT[i][j][0] * (1/self.fps[i])*1000 - \
                                                   self.var_head_timing_RIGHT[i][j][0] * (1/self.fps[i])*1000))
                    worksheet.write(cnt, 15, self.gain_device_RIGHT[i][j])
                    worksheet.write(cnt, 16, self.gain_manual_RIGHT[i][j])
                    worksheet.write(cnt, 17, self.PR1_RIGHT[i][0])
                    worksheet.write(cnt, 18, self.PR2_RIGHT[i][0])
                    worksheet.write(cnt, 19, self.PR_global_RIGHT[i])
                    worksheet.write(cnt, 20, "")
                    worksheet.write(cnt, 21, "")
                    worksheet.write(cnt, 22, "")
                    worksheet.write(cnt, 23, "")
                    worksheet.write(cnt, 24, "")
                    worksheet.write(cnt, 25, "")
                    cnt += 1
                else:
                    for key in self.var_onset_offset_RIGHT[i][j].keys():
                        worksheet.write(cnt, 0, self.names[i])
                        worksheet.write(cnt, 1, self.HIMPorSHIMP_text)
                        worksheet.write(cnt, 2, self.plane_text)
                        worksheet.write(cnt, 3, len(self.traces_RIGHT[i]))
                        worksheet.write(cnt, 4, j+1)
                        worksheet.write(cnt, 5, "RIGHT")
                        worksheet.write(cnt, 6, bool(self.var_imp_take_auto_RIGHT[i][j][3]))
                        worksheet.write(cnt, 7, bool(self.var_imp_take_auto_RIGHT[i][j][1]))
                        worksheet.write(cnt, 8, bool(self.var_imp_take_auto_RIGHT[i][j][0]))
                        worksheet.write(cnt, 9, bool(self.var_imp_take_auto_RIGHT[i][j][2]))
                        worksheet.write(cnt, 10, bool(self.var_imp_take_RIGHT[i][j].get()))

                        worksheet.write(cnt, 11, round(self.var_head_timing_RIGHT[i][j][0] * (1 / self.fps[i]) * 1000))
                        worksheet.write(cnt, 12, round(self.var_head_timing_RIGHT[i][j][1] * (1 / self.fps[i]) * 1000))

                        worksheet.write(cnt, 13, round(self.var_head_peak_RIGHT[i][j][1]))
                        worksheet.write(cnt, 14, round(self.var_head_peak_RIGHT[i][j][0] * (1 / self.fps[i])*1000 - \
                                                       self.var_head_timing_RIGHT[i][j][0] * (1/self.fps[i])*1000))
                        worksheet.write(cnt, 15, self.gain_device_RIGHT[i][j])
                        worksheet.write(cnt, 16, self.gain_manual_RIGHT[i][j])
                        worksheet.write(cnt, 17, self.PR1_RIGHT[i][0])
                        worksheet.write(cnt, 18, self.PR2_RIGHT[i][0])
                        worksheet.write(cnt, 19, self.PR_global_RIGHT[i])
                        worksheet.write(cnt, 20, True if (self.traces_RIGHT[i][j][1][round(self.sacc_peak_RIGHT[i][j][key]*self.fps[i]/1000)] > 0 and \
                                                         self.HIMPorSHIMP_text == "VW_HITest") or \
                                                         (self.traces_RIGHT[i][j][1][round(self.sacc_peak_RIGHT[i][j][key]*self.fps[i]/1000)] < 0 and \
                                                          self.HIMPorSHIMP_text == "VW_SHIMPTest") \
                                                 else False)
                        worksheet.write(cnt, 21, round(self.var_onset_offset_RIGHT[i][j][key][0].get() - self.var_head_timing_RIGHT[i][j][0]*(1/self.fps[i])*1000))
                        worksheet.write(cnt, 22, round(self.var_onset_offset_RIGHT[i][j][key][1].get() - self.var_head_timing_RIGHT[i][j][0]*(1/self.fps[i])*1000))

                        worksheet.write(cnt, 23, self.sacc_peak_RIGHT[i][j][key] - round(self.var_head_timing_RIGHT[i][j][0]*(1/self.fps[i])*1000))

                        worksheet.write(cnt, 24, round(abs(self.traces_RIGHT[i][j][1][round(self.sacc_peak_RIGHT[i][j][key]*self.fps[i]/1000)])))
                        worksheet.write(cnt, 25, self.sacc_peak_RIGHT[i][j][key]/round(self.var_head_peak_RIGHT[i][j][1]))
                        cnt += 1
        workbook.close()


## create main window
root = tk.Tk()
# name this window
root.title("Preprocessing vHIT")
# icon for title
root.iconbitmap('Muha1024.ico')
# create loop to rum program
GUI_vHIT_App = GUI_vHIT(root)
root.mainloop()
