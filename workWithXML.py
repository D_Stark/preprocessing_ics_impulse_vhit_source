from xml.etree import ElementTree
import re

def parse_xml(path, file):
    tree = ElementTree.parse(path+"/"+file)
    return tree

def get_pref(root):
    match = re.search(r"^\{(.+?)\}", root.tag)
    if match:
        pref = match.group(0)
    else:
        pref = ""
    return pref

def get_names(root, pref):
    # function get_names() return first name as str
    for name in root.iter('{}FirstName'.format(pref)):
                return name.text


def get_sessions(root, pref, HIMPorSHIMP_text, plane_text):
    # function returns list where each element is one session
    sessions = []
    for HITest in root.iter('{}{}'.format(pref, HIMPorSHIMP_text)):
        for plane in HITest.iter('{}TestType'.format(pref)):
            if plane.text == plane_text:
                sessions.append(HITest)
    return sessions

def get_avg_framerate(HITest, pref):
    fps = int(HITest[0].find('{}AvgFrameRate'.format(pref)).text)
    return  fps

def get_split_impulses(HITest, pref, HIMPorSHIMP_text):
    # function returns two lists where each element is one impulse for LEFT and RIGHT respectively
    # if no sessions exists, return empty list
    impulses_LEFT = []
    impulses_RIGHT = []
    # check test to accordintly extract impulses
    if HIMPorSHIMP_text == "VW_HITest":
        test = "VW_HIImpulse"
    else:
        test = "VW_SHIMPImpulse"
    if HITest != []:
        for impulse in HITest[0].iter('{}{}'.format(pref, test)):
            for side in impulse.iter('{}IsDirectionLeft'.format(pref)):
                if side.text == "true":
                    impulses_LEFT.append(impulse)
                else:
                    impulses_RIGHT.append(impulse)
        return impulses_LEFT, impulses_RIGHT
    else:
        return [], []

def get_traces(impulse, pref):
    for trace in impulse.iter('{}HeadVelocitySamples'.format(pref)):
        head = list(map(float, trace.text.replace(",", ".").split(";")))
    for trace in impulse.iter('{}EyeVelocitySamples'.format(pref)):
        eye = list(map(float, trace.text.replace(",", ".").split(";")))
    return head, eye

def get_gain_device(impulses, pref):
    for impulse in impulses.iter('{}Gain'.format(pref)):
        gain = round(int(impulse.text)/10000, 2)
    return gain