## The program is in progress ##

The program pre-processes *.xml files exported from the ICS Impulse software.

The files contain eye and head velocity traces for each vHIT impulse.

The program exports an *.xlsx file containing VOR gain and saccadic eye response data.

For detailed information read UserGuide.pdf     

Installation:

Clone the project. 
Install packages 

Linux		python3 -m pip3 install -r requirements.txt
also tkinter	apt-get update
		apt-get install python-tk


Windows	py -m pip install -r requirements.txt
