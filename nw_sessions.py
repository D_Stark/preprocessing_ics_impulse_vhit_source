import tkinter as tk
import get_traces
import plotting
import analysis
import workWithXML

def create_nw_session(self):
    # create new window
    self.nw_session = tk.Toplevel(self.master)
    self.nw_session.iconbitmap('Muha1024.ico')
    # bind event "close" with function on_closing_nw_session
    self.nw_session.protocol("WM_DELETE_WINDOW", lambda: on_closing_nw_session(self))
    # set new window title
    self.nw_session.title("Session selection")
    # set geometry of new window
    self.nw_session.geometry("{}x{}".format(int(self.width / 100 * 25), int(self.height / 100 * 30)))
    # set iterator to select subjects equal from sessions_ind
    self.name_order = 0
    # create label showing selected subject's name
    self.subject_name = tk.Label(self.nw_session,
                                 text="Subject's name: {}".format(self.names[self.sessions_ind[self.name_order]]))
    # create label showing number of subject who has more than one session
    self.subject_nubmer = tk.Label(self.nw_session,
                                   text="Subject {} from {}".format(self.name_order + 1, len(self.sessions_ind)))
    # button to switch to next subject, button calls fun_btn_next()
    self.nw_btn_next = tk.Button(self.nw_session, width=10, text=">>", borderwidth=3, command=lambda: fun_nw_btn_next(self))
    # button to switch to previous subject, button calls fun_btn_prev()
    self.nw_btn_prev = tk.Button(self.nw_session, width=10, text="<<", borderwidth=3, command=lambda: fun_nw_btn_prev(self),
                                 state="disabled")
    # create tuple of tk variables per subject and which value can be traced
    # and it shows which test to use
    self.sessions_val = tuple(tk.IntVar() for i in range(len(self.sessions_ind)))
    # set these values to number of last session
    for i in range(len(self.sessions_val)):
        self.sessions_val[i].set(self.sessions_number[self.sessions_ind[i]] - 1)
    # pack label, subject name, these two buttons, frame_sessions, and radiobuttons into new window
    self.subject_name.grid(row=0, column=0, columnspan=2, sticky="W")
    self.subject_nubmer.grid(row=0, column=2, columnspan=2, sticky="W")
    self.nw_btn_prev.grid(row=1, column=2, sticky="WN")
    self.nw_btn_next.grid(row=1, column=3, sticky="WN")
    # deactivate buttons if only one subject has multiple sessions
    if len(self.sessions_ind) == 1:
        self.nw_btn_next.configure(state="disabled")
    # create frame and set of radiobuttons to select session per subject
    nw_session_rud_but_frame(self)

def fun_nw_btn_next(self):
    # if selected subject is NOT last in list
    if self.name_order < (len(self.sessions_ind) - 1):
        # increment iterator name_order by 1
        self.name_order = self.name_order + 1
        # if this is last subject deactivate btn_next
        if self.name_order == (len(self.sessions_ind) - 1):
            self.nw_btn_next.configure(state="disabled")
        # if this is second subject activate btn_prev
        if self.name_order == 1:
            self.nw_btn_prev.configure(state="active")
        # reconfigure text of label showing subject's name
        self.subject_name.configure(text="Subject's name: {}".format(self.names[self.sessions_ind[self.name_order]]))
        # reconfigure text of label showing subject's number
        self.subject_nubmer.configure(text="Subject {} from {}".format(self.name_order + 1, len(self.sessions_ind)))
    # destroy frame_sessions with all radiobuttons and create them over with already updated name_order
    self.frame_session1.destroy()
    self.frame_session2.destroy()
    nw_session_rud_but_frame(self)

def fun_nw_btn_prev(self):
    # if selected subject is NOT first in list
    if self.name_order > 0:
        # decrement iterator name_order by 1
        self.name_order = self.name_order - 1
        # if this is first subject deactivate btn_prev
        if self.name_order == 0:
            self.nw_btn_prev.configure(state="disabled")
        # if this is one before last subject activate btn_next
        if self.name_order == (len(self.sessions_ind) - 2):
            self.nw_btn_next.configure(state="active")
        # reconfigure text of label showing subject's name
        self.subject_name.configure(text="Subject's name: {}".format(self.names[self.sessions_ind[self.name_order]]))
        # reconfigure text of label showing subject's number
        self.subject_nubmer.configure(text="Subject {} from {}".format(self.name_order + 1, len(self.sessions_ind)))
    # destroy frame_sessions with all radiobuttons and create them again with already updated name_order
    self.frame_session1.destroy()
    self.frame_session2.destroy()
    nw_session_rud_but_frame(self)

def nw_session_rud_but_frame(self):
    # create set of radio buttons to select session
    # but first create frame to pack these radiobuttons into it
    self.frame_session1 = tk.Frame(self.nw_session)
    # create radio buttons per subject
    # double loop first to crete element per subject which contains as many radiobuttons as number of sessions
    self.RadBut_sessions = tuple(tuple(tk.Radiobutton(self.frame_session1, text=str(j + 1), variable=self.sessions_val[i], value=j)
                                       for j in range(self.sessions_number[self.sessions_ind[i]]))
                                 for i in range(len(self.sessions_ind)))
    # create labels per subject showing time of each session
    # but first create frame to pack these labels into it
    self.frame_session2 = tk.Frame(self.nw_session)
    # double loop first to crete element per subject which contains as many labels as number of sessions
    self.label_sessions = tuple(tuple(tk.Label(self.frame_session2,
                                               text="{} {}-{}".format(self.sessions[i][j].find(
                                                   '{}StartDateTime'.format(self.pref[i])).text[:10],
                                                                      self.sessions[i][j].find(
                                                                          '{}StartDateTime'.format(self.pref[i])).text[
                                                                      11:19],
                                                                      self.sessions[i][j].find(
                                                                          '{}EndDateTime'.format(self.pref[i])).text[
                                                                      11:19]))
                                      for j in range(self.sessions_number[i]))
                                for i in self.sessions_ind)
    # pack frame_session1 with radiobuttons into nw_session
    self.frame_session1.grid(row=1, column=0)
    # pack radiobuttons into frame_session1
    for i in range(len(self.RadBut_sessions[self.name_order])):
        self.RadBut_sessions[self.name_order][i].pack()
    # pack frame_session2 with label into nw_session
    self.frame_session2.grid(row=1, column=1)
    # pack label_sessions into frame_session2
    for i in range(len(self.label_sessions[self.name_order])):
        self.label_sessions[self.name_order][i].pack()



def on_closing_nw_session(self):
    # destroy and delete all widgets of second window
    self.nw_session.destroy()
    self.frame_session1.destroy()
    self.frame_session2.destroy()
    self.nw_btn_next.destroy()
    self.nw_btn_prev.destroy()
    self.subject_nubmer.destroy()
    self.subject_name.destroy()
    del self.nw_session, self.frame_session1, self.frame_session2, self.RadBut_sessions, self.label_sessions
    del self.nw_btn_next, self.nw_btn_prev, self.subject_nubmer, self.subject_name
    # delete unchosen sessions in each subject by rewriting elements in sessions
    for i in range(len(self.sessions_ind)):
        self.sessions[self.sessions_ind[i]] = [self.sessions[self.sessions_ind[i]][self.sessions_val[i].get()]]
    # but first reset name_order to start from first subject
    self.name_order = 0
    # and imp_order to start from first impulse
    self.imp_order_LEFT = 0
    self.imp_order_RIGHT = 0
    # get average framerate per session
    get_traces.get_framerate(self)
    # extract impulses to LEFT and RIGHT per subject
    # first variable get_impulses returns is list of impulses
    # each element of list is list where first variable is impulses to LEFT, second variable - to RIGHT
    get_traces.get_impulses_traces_time(self)
    # create variables storing whether remove or take imp for all subjects
    self.var_imp_take_LEFT = tuple([tk.IntVar(value=1) for j in range(len(self.traces_LEFT[i]))]
                                   for i in range(len(self.sessions)))
    self.var_imp_take_RIGHT = tuple([tk.IntVar(value=1) for j in range(len(self.traces_RIGHT[i]))]
                                    for i in range(len(self.sessions)))
    # get gain per impulse calculated by device
    self.gain_device_LEFT = (tuple(tuple(workWithXML.get_gain_device(self.impulses[i][0][j], self.pref[i])
                                         for j in range(len(self.impulses[i][0])))
                                   for i in range(len(self.sessions))))
    self.gain_device_RIGHT = (tuple(tuple(workWithXML.get_gain_device(self.impulses[i][1][j], self.pref[i])
                                         for j in range(len(self.impulses[i][1])))
                                   for i in range(len(self.sessions))))

    # check if LEFT impulses exist to activate switching button
    if self.traces_LEFT[self.name_order] != []:
        self.btn_next_imp_LEFT.configure(state="active")
        # also call function to create checkbox and assign corresponding imp to it
        self.fun_imp_take_ckb_create_LEFT()

    # check if RIGHT impulses exist to activate switching button
    if self.traces_RIGHT[self.name_order] != []:
        self.btn_next_imp_RIGHT.configure(state="active")
        # call function to create checkbox and assign corresponding variable to it
        self.fun_imp_take_ckb_create_RIGHT()

    # create list of colors to paint saccades and corresponding buttons removing them
    self.colors = ["#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE",
                   "#DC143C", "#008B45", "#FFA500", "#B0171F", "#8B008B", "#00CD66", "#EE00EE"]

    ## for LEFT
    # get head peaks
    # variable structure: [subject][impulse][index, value]
    self.var_head_peak_LEFT = tuple([analysis.head_peak(self.traces_LEFT[i][j][0])
                                     for j in range(len(self.traces_LEFT[i]))]
                                    for i in range(len(self.sessions)))

    # get head onset and offset
    # variable structure: [subject][impulse][onset, offset]
    self.var_head_timing_LEFT = tuple([[analysis.head_onset(self.traces_LEFT[i][j][0], self.fps[i]),
                                        analysis.head_offset(self.traces_LEFT[i][j][0])]
                                       for j in range(len(self.traces_LEFT[i]))]
                                      for i in range(len(self.sessions)))

    # check different criteria to remove impulse
    # variable structure of var_imp_take_auto: [subject][impulse][head peak < threshold,
    #                                                             head peak out of +-3sd of trial
    #                                                               (see var_head_3sd_LEFT and head_out_3sd),
    #                                                             no head offset,
    #                                                             head bounce > 50% of head peak]
    # for LEFT
    # but first get interval calculated as:
    # 1) mean of the interval prior 80msec to and 120msec after peak head calculated for each impulse
    # 2) MEAN of all such means of all impulses
    # 3) std of all such means of all impulses
    # 4) interval [MEAN - 3std, MEAN + 3std]
    self.var_head_3sd_LEFT = tuple(analysis.head_peak_3sdt(self.traces_LEFT[i], self.fps[i])
                                   for i in range(len(self.sessions)))
    # check each criterion
    self.var_imp_take_auto_LEFT = tuple(
        [[analysis.head_less_th(self.traces_LEFT[i][j][0], self.var_head_th.get()),
          analysis.head_out_3sd(self.traces_LEFT[i][j][0], self.var_head_3sd_LEFT[i],
                                self.fps[i]),
          analysis.head_no_offset(self.var_head_timing_LEFT[i][j][1]),
          analysis.head_bounce(self.traces_LEFT[i][j][0])]
         for j in range(len(self.traces_LEFT[i]))]
        for i in range(len(self.sessions)))

    # extract saccades
    self.saccades_LEFT = tuple(
        [analysis.saccades(self.traces_LEFT[i][j][1], self.var_head_timing_LEFT[i][j][0]+round(60/((1/self.fps[i])*1000)),
                           self.fps[i], self.var_sac_th.get())
         for j in range(len(self.traces_LEFT[i]))]
        for i in range(len(self.sessions)))

    # add form to edit or remove extracted saccades
    # but first create variable to track onsets and offsets
    # these variables are created each time when impulse or subject are changed
    self.var_onset_offset_LEFT = [[{} for j in range(len(self.traces_LEFT[i]))]
                                  for i in range(len(self.sessions))]

    # copy saccades to editable variable
    for i in range(len(self.saccades_LEFT)):
        for j in range(len(self.saccades_LEFT[i])):
            for k in range(len(self.saccades_LEFT[i][j])):
                self.var_onset_offset_LEFT[i][j][k] = [tk.IntVar(value=round(self.saccades_LEFT[i][j][k][0]*(1/self.fps[i])*1000)),
                                                       tk.IntVar(value=round(self.saccades_LEFT[i][j][k][1]*(1/self.fps[i])*1000))]

    # get positions of saccades peaks
    self.sacc_peak_LEFT = [[{} for j in range(len(self.traces_LEFT[i]))]
                           for i in range(len(self.sessions))]

    for i in range(len(self.saccades_LEFT)):
        for j in range(len(self.saccades_LEFT[i])):
            for key in self.var_onset_offset_LEFT[i][j].keys():
                self.sacc_peak_LEFT[i][j][key] = analysis.saccade_peak(self.traces_LEFT[i][j][1],
                                                                        self.var_onset_offset_LEFT[i][j][key][0].get(),
                                                                        self.var_onset_offset_LEFT[i][j][key][1].get(),
                                                                        self.fps[i])
    # self.sacc_peak_LEFT = [[[analysis.saccade_peak(self.traces_LEFT[i][j][1],
    #                                                 self.var_onset_offset_LEFT[i][j][key][0].get(),
    #                                                 self.var_onset_offset_LEFT[i][j][key][1].get(),
    #                                                 self.fps[i])
    #                           for key in self.var_onset_offset_LEFT[i][j].keys()]
    #                          for j in range(len(self.var_onset_offset_LEFT[i]))]
    #                         for i in range(len(self.sessions))]

    # also create variable to track if these forms created first time fot given subject and impulse
    # variable equal to 0 means first time, 1 - other times
    self.var_form_order_LEFT = [[0 for j in range(len(self.traces_LEFT[i]))]
                                for i in range(len(self.sessions))]
    # create variable to be used as key in dictionary where edited saccades are stored
    self.num_added_sac_LEFT = [[0 for j in range(len(self.traces_LEFT[i]))]
                               for i in range(len(self.sessions))]

    if self.traces_LEFT[self.name_order] != []:
        # create editable form for each saccade of selected subject and impulse
        for key in self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT].keys():
            self.fun_btn_add_sac_LEFT(self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][0].get(),
                                      self.var_onset_offset_LEFT[self.name_order][self.imp_order_LEFT][key][1].get(),
                                      key=key, who=1)
        # reset var_form_form_order for first subject first impulse
        self.var_form_order_LEFT[self.name_order][self.imp_order_LEFT] = 1

    ## for RIGHT
    # get head peaks
    # variable structure: [subject][impulse][index, value]
    self.var_head_peak_RIGHT = tuple([analysis.head_peak(self.traces_RIGHT[i][j][0])
                                      for j in range(len(self.traces_RIGHT[i]))]
                                     for i in range(len(self.sessions)))

    # get head onset and offset
    # variable structure: [subject][impulse][onset, offset]
    self.var_head_timing_RIGHT = tuple(
        [[analysis.head_onset(self.traces_RIGHT[i][j][0], self.fps[i]),
          analysis.head_offset(self.traces_RIGHT[i][j][0])]
         for j in range(len(self.traces_RIGHT[i]))]
        for i in range(len(self.sessions)))

    # check different criteria to remove impulse
    # variable structure of var_imp_take_auto: [subject][impulse][head peak < threshold,
    #                                                             head peak out of +-3sd of trial
    #                                                               (see var_head_3sd_LEFT and head_out_3sd),
    #                                                             no head offset,
    #                                                             head bounce > 50% of head peak]
    #
    # but first get interval calculated as:
    # 1) mean of the interval prior 80msec to and 120msec after peak head calculated for each impulse
    # 2) MEAN of all such means of all impulses
    # 3) std of all such means of all impulses
    # 4) interval [MEAN - 3std, MEAN + 3std]
    self.var_head_3sd_RIGHT = tuple(analysis.head_peak_3sdt(self.traces_RIGHT[i], self.fps[i])
                                    for i in range(len(self.sessions)))
    # check each criterion
    self.var_imp_take_auto_RIGHT = tuple(
        [[analysis.head_less_th(self.traces_RIGHT[i][j][0], self.var_head_th.get()),
          analysis.head_out_3sd(self.traces_RIGHT[i][j][0], self.var_head_3sd_RIGHT[i],
                                self.fps[i]),
          analysis.head_no_offset(self.var_head_timing_RIGHT[i][j][1]),
          analysis.head_bounce(self.traces_RIGHT[i][j][0])]
         for j in range(len(self.traces_RIGHT[i]))]
        for i in range(len(self.sessions)))

    # extract saccades
    self.saccades_RIGHT = tuple(
        [analysis.saccades(self.traces_RIGHT[i][j][1], self.var_head_timing_RIGHT[i][j][0]+round(60/((1/self.fps[i])*1000)),
                           self.fps[i], self.var_sac_th.get())
         for j in range(len(self.traces_RIGHT[i]))]
        for i in range(len(self.sessions)))

    # add form to edit or remove extracted saccades
    # but first create variable to track onsets and offsets
    # these variables are created each time when impulse or subject are changed
    self.var_onset_offset_RIGHT = [[{} for j in range(len(self.traces_RIGHT[i]))]
                                   for i in range(len(self.sessions))]
    # copy saccades to editable variable
    for i in range(len(self.saccades_RIGHT)):
        for j in range(len(self.saccades_RIGHT[i])):
            for k in range(len(self.saccades_RIGHT[i][j])):
                self.var_onset_offset_RIGHT[i][j][k] = [tk.IntVar(value=round(self.saccades_RIGHT[i][j][k][0]*(1/self.fps[i])*1000)),
                                                       tk.IntVar(value=round(self.saccades_RIGHT[i][j][k][1]*(1/self.fps[i])*1000))]

    # get positions of saccades peaks
    self.sacc_peak_RIGHT = [[{} for j in range(len(self.traces_RIGHT[i]))]
                            for i in range(len(self.sessions))]

    for i in range(len(self.saccades_RIGHT)):
        for j in range(len(self.saccades_RIGHT[i])):
            for key in self.var_onset_offset_RIGHT[i][j].keys():
                self.sacc_peak_RIGHT[i][j][key] = analysis.saccade_peak(self.traces_RIGHT[i][j][1],
                                                                        self.var_onset_offset_RIGHT[i][j][key][0].get(),
                                                                        self.var_onset_offset_RIGHT[i][j][key][1].get(),
                                                                        self.fps[i])

    # also create variable to track if these forms created first time fot given subject and impulse
    # variable equal to 0 means first time, 1 - other times
    self.var_form_order_RIGHT = [[0 for j in range(len(self.traces_RIGHT[i]))]
                                 for i in range(len(self.sessions))]
    # create variable to be used as key in dictionary where edited saccades are stored
    self.num_added_sac_RIGHT = [[0 for j in range(len(self.traces_RIGHT[i]))]
                                for i in range(len(self.sessions))]

    if self.traces_RIGHT[self.name_order] != []:
        # create editable form for each saccade of selected subject and impulse
        for key in self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT].keys():
            self.fun_btn_add_sac_RIGHT(self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][0],
                                       self.var_onset_offset_RIGHT[self.name_order][self.imp_order_RIGHT][key][1],
                                       key=key, who=1)
        # reset var_form_form_order for first subject first impulse
        self.var_form_order_RIGHT[self.name_order][self.imp_order_RIGHT] = 1

    # calculate gain for each desaccaded impulse
    # for LEFT
    self.gain_manual_LEFT = [[analysis.calculate_gain(self.traces_LEFT[i][j][0], self.traces_LEFT[i][j][1],
                                                      self.var_head_timing_LEFT[i][j][0],
                                                      self.var_head_timing_LEFT[i][j][1],
                                                      self.var_onset_offset_LEFT[i][j], self.fps[i])
                              for j in range(len(self.traces_LEFT[i]))]
                             for i in range(len(self.traces_LEFT))]

    # for RIGHT
    self.gain_manual_RIGHT = [[analysis.calculate_gain(self.traces_RIGHT[i][j][0], self.traces_RIGHT[i][j][1],
                                                      self.var_head_timing_RIGHT[i][j][0],
                                                      self.var_head_timing_RIGHT[i][j][1],
                                                      self.var_onset_offset_RIGHT[i][j], self.fps[i])
                              for j in range(len(self.traces_RIGHT[i]))]
                             for i in range(len(self.sessions))]

    ## get PR scores for
    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_LEFT = [analysis.calculate_PR1_peak(self.sacc_peak_LEFT[i],
                                                 self.var_imp_take_auto_LEFT[i],
                                                 self.var_imp_take_LEFT[i])
                     for i in range(len(self.sessions))]
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_LEFT = [analysis.calculate_PR2_peak(self.sacc_peak_LEFT[i],
                                                 self.var_imp_take_auto_LEFT[i],
                                                 self.var_imp_take_LEFT[i])
                     for i in range(len(self.sessions))]
    # global
    self.PR_global_LEFT = [analysis.calculate_PR_global(self.PR1_LEFT[i][0], self.PR2_LEFT[i][0],
                                                        self.PR1_LEFT[i][1], self.PR2_LEFT[i][1])
                           for i in range(len(self.sessions))]

    # first order saccades with following structure: [subject][PR1, number of first order saccades]
    self.PR1_RIGHT = [analysis.calculate_PR1_peak(self.sacc_peak_RIGHT[i],
                                                  self.var_imp_take_auto_RIGHT[i],
                                                  self.var_imp_take_RIGHT[i])
                      for i in range(len(self.sessions))]
    # second order saccades with following structure: [subject][PR2, number of second order saccades]
    self.PR2_RIGHT = [analysis.calculate_PR2_peak(self.sacc_peak_RIGHT[i],
                                                  self.var_imp_take_auto_RIGHT[i],
                                                  self.var_imp_take_RIGHT[i])
                      for i in range(len(self.sessions))]
    # global
    self.PR_global_RIGHT = [analysis.calculate_PR_global(self.PR1_RIGHT[i][0], self.PR2_RIGHT[i][0],
                                                         self.PR1_RIGHT[i][1], self.PR2_RIGHT[i][1])
                            for i in range(len(self.sessions))]

    ## start plotting
    # plotting function automatically detect empty sessions
    plotting.plotting(self, who=0)

    # activate button Export
    self.btn_export.configure(state="active")