import workWithXML
import analysis

def get_impulses_traces_time(self):
    self.impulses = []
    self.traces_LEFT = []
    self.traces_RIGHT = []
    self.time_LEFT = []
    self.time_RIGHT = []
    for i in range(len(self.sessions)):
        if self.sessions[i] != []:
            # extract impulses to LEFT and RIGHT per subject
            # first variable get_split_impulses returns list of impulses to LEFT, second variable - to RIGHT
            self.impulses.append(workWithXML.get_split_impulses(self.sessions[i], self.pref[i], self.HIMPorSHIMP_text))

            # get traces for LEFT and RIGHT separately
            # both variables have following structure: [subject #][impulse #][head - 0, eye - 1]
            self.traces_LEFT.append(
                [workWithXML.get_traces(self.impulses[i][0][j], self.pref[i]) for j in range(len(self.impulses[i][0]))])
            self.traces_RIGHT.append(
                [workWithXML.get_traces(self.impulses[i][1][j], self.pref[i]) for j in range(len(self.impulses[i][1]))])
            # calculate time for each impulse using average framerate (fps)
            # output is list of time points (msec) цшер following structure: [subject #][impulse #]
            self.time_LEFT.append([analysis.calculate_time(self.traces_LEFT[i][j][0], self.fps[i]) for j in
                                   range(len(self.traces_LEFT[i]))])
            self.time_RIGHT.append([analysis.calculate_time(self.traces_RIGHT[i][j][0], self.fps[i]) for j in
                                    range(len(self.traces_RIGHT[i]))])
        else:
            self.impulses.append([[],[]])
            self.traces_LEFT.append([])
            self.traces_RIGHT.append([])
            self.time_LEFT.append([])
            self.time_RIGHT.append([])

def get_framerate(self):
    self.fps = []
    for i in range(len(self.sessions)):
        if self.sessions[i] != []:
            self.fps.append(workWithXML.get_avg_framerate(self.sessions[i], self.pref[i]))
        else:
            self.fps.append([])